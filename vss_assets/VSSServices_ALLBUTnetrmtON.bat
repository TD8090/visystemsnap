::______WINDOWS FEATURES
::~ r05: Windows Search - Provides content indexing, property caching, and search results for files, e-mail, and other content
::||sc config WSearch start= auto
::~ r0: ActiveX Installer - turns over the security settings to the individual application IE,
::~       Office, etc.  Security risk, and tied to UAC so is unneeded/unused if UAC disabled
sc config AxInstSV start= auto
::~ r0: Application Experience - app launch compat checks
sc config AeLookupSvc start= auto
::~ r0: Diagnostic Policy Service - problm det, trshooting and resolutn for Win components(diagnostcs will no longer function)
sc config DPS start= auto
::~ r0: Parental Controls - backward compat of Parental Control from Vista
sc config WPCSvc start= auto
::~ r0: Program Compatibility Assistant Service - dialogues after installations finish
sc config PcaSvc start= auto
::~ r0: Portable Device Enumerator Service - Enforces group policy for removable mass-storage devices. 
::~  	    Enables apps like WMPlayer and Image Import Wizard to transfer/synch content
sc config WPDBusEnum start= auto
::~ r0: Media Center Extender Service - Allows Media Center Extenders to locate and connect to the computer
sc config Mcx2Svc start= auto
::~ r0: Windows Media Player Network Sharing Service - Shares WMP libs to other networked players and media devices
sc config WMPNetworkSvc start= auto

::______WINDOWS SECURITY
::~ r3: Secondary Logon - (??Disables Run As Admin??)(allows a “limited user” account to start an application 
::~       or process with higher privileges, such as the Administrator account or another user)
sc config seclogon start= auto
::~ r04: Security Center - monitors and reports security health settings like firewall, antivirus, antispyware, WinUpdate, 
::~       UAC, and Inet settings.  Provides COM APIs for indep soft. vendors to register and record app state 
::~       to this service.  Enables Action Center UI to provide systray alerts and a graphical view of security health 
::~       states in the ActionCtr contpanel. Network Access Protection (NAP) uses this to report 
::~       the security health states of clients to the NAP Network Policy Server to make network quarantine decisions
sc config wscsvc start= auto
::~ r0: Error Reporting - sends data to MS 
sc config WerSvc start= auto
::~ r0: Problem Reports - sends data to MS
sc config wercplsupport start= auto
::~ r2: Protected Storage - you may need this service to manage private keys for encryption purposes
sc config ProtectedStorage start= auto

::______TABLET/PHONE/TV/RADIO/FAX/PRINT
::~ r0: tablet - uses system resources with no possible benefit
sc config TabletInputService start= auto
::~ r0: dial-up modem connectivity
sc config TapiSrv start= auto
::~ r0: TV and FM broadcast reception        
sc config ehRecvr start= auto
::~ r0: TV programs recorder
sc config ehSched start= auto
::~ r0: Fax - send and receive faxes
sc config Fax start= auto
::~ r0: Print Spooler - DISABLE IF: No Printer 
sc config Spooler start= auto


::______SMART CARD AUTHENTICATION
::~ r0: Smart Card - disables authentication with smart cards(not same as flash media)
sc config SCardSvr start= auto
::~ r0: Smart Card Removal Policy - allows system lock upon smart card removal
sc config SCPolicySvc start= auto

::______ICS
::~ r1: Internet Connection Sharing - use a router instead, ICS is for routing connection through a PC to 1 or more children.
sc config SharedAccess start= auto
::~ r1: Provides support for 3rd party protocol plug-ins for Internet Connection Sharing (ICS).
sc config ALG start= auto
PAUSE