:: create scheme based on High Performance, set as active, and go to town on that nasty
@Set _ViPwrScheme=9d7849b6-0359-4a43-97ae-7e4cf5afb9c3
@Set _vischeme="Vi Power Scheme"
@Powercfg.exe -l | find /i %_vischeme%
@if %errorlevel% EQU 1  (
	ECHO "Vi Power Scheme does not exist, creating now..."
	@powercfg -DUPLICATESCHEME SCHEME_MIN %_ViPwrScheme%
	@powercfg -CHANGENAME %_ViPwrScheme% %_vischeme% "Visual Impressions Digital Signage Deployment Scheme."
)
@Powercfg /S %_ViPwrScheme%
@ECHO ^|
@ECHO Require password on wakeup -^> No
@POWERCFG -SETACVALUEINDEX %_ViPwrScheme% fea3413e-7e05-4911-9a71-700331f1c294 0e796bdb-100d-47d6-a2d5-f7d2daa51f51 000

@ECHO HDD Sleep After -^> Never
@POWERCFG -SETACVALUEINDEX %_ViPwrScheme% 0012ee47-9041-4b5d-9b77-535fba8b1442 6738e2c4-e8a5-4a42-b16a-e040e769756e 000

@ECHO Wireless Adapter Power Saving -^> Maximum Performance
@POWERCFG -SETACVALUEINDEX %_ViPwrScheme% 19cbb8fa-5279-450e-9fac-8a3d5fedd0c1 12bbebe6-58d6-4636-95bb-3217ef867c1a 000

@ECHO PC Sleep after -^> Never
@POWERCFG -SETACVALUEINDEX %_ViPwrScheme% 238c9fa8-0aad-41ed-83f4-97be242c8f20 29f6c1db-86da-48c5-9fdb-f2b67b1f44da 000

@ECHO Hibernate after -^> Never
@POWERCFG -SETACVALUEINDEX %_ViPwrScheme% 238c9fa8-0aad-41ed-83f4-97be242c8f20 9d7815a6-7ee4-497e-8888-515a05f02364 000

@ECHO Allow hybrid sleep -^> Disable
@POWERCFG -SETACVALUEINDEX %_ViPwrScheme% 238c9fa8-0aad-41ed-83f4-97be242c8f20 94ac6d29-73ce-41a6-809f-6363ba21b47e 000

@ECHO Power button action -^> Shutdown
@POWERCFG -SETACVALUEINDEX %_ViPwrScheme% 4f971e89-eebd-4455-a8de-9e59040e7347 7648efa3-dd9c-4e3e-b566-50f929386280 003

@ECHO Sleep button action -^> Do nothing
@POWERCFG -SETACVALUEINDEX %_ViPwrScheme% 4f971e89-eebd-4455-a8de-9e59040e7347 96996bc0-ad50-47ec-923b-6f41874dd9eb 000

@ECHO Display off timer -^> Never
@POWERCFG -SETACVALUEINDEX %_ViPwrScheme% 7516b95f-f776-4464-8c53-06167f40cc99 3c0bc021-c8a8-4e07-a973-6b14cbcb2b7e 000

@pause