::______NETWORK 1 (FLUFF)
::~ r0: Offline Files - (OF) synchronizes files across network. Found in Windows client & server OSs, 
sc config CscService start= auto
::~ r1: HomeGroup Listener - config and maint of the homegroup-joined computer
sc config HomeGroupListener start= auto
::~ r1: HomeGroup Provider - networking tasks for config and maint of homegroups
sc config HomeGroupProvider start= auto
::~ r1: Network Access Protection Agent - collects and manages health policy info for client computers on a network in network-based remediation
sc config napagent start= auto
::~ r1: Distributed Link Tracking Client - Upon symlinking across a network, If a file on PC-A is moved, this would tell PC-B to update the symlink
sc config TrkWks start= auto		

::______NETWORK 2 (USEFUL)
::~ r2: IP Helper - leveraged in IPv6 transitions
sc config iphlpsvc start= auto
::__r2: Computer Browser (ENABLE IF: networked) No lag time is discernible if this service remains disabled on all but one computer
sc config Browser start= auto

::______NETWORK 3 (CORE) 
::~ r3: Function Discovery Resource Publication	- Publishes PC and its attached resources so they can be discovered over network					
sc config FDResPub start= auto
::~ r3: Server (ENABLE IF: networked) supports file, print, and named-pipe sharing over the network														
sc config LanmanServer start= auto
::~ r4: SSDP Discovery	(required to see networked resources (Network Discovery) in the �Network� display pane)
::~       (Discovers networked devices and services that use the SSDP discovery protocol, such as UPnP devices)										
sc config SSDPSRV start= auto
::~ r3: TCP/IP NetBIOS Helper (ENABLE IF: logon/share in workgroup network) (Provides support for the NetBIOS over TCP/IP (NetBT) service and 
::~       NetBIOS name resolution for clients on the network, therefore enabling users to share files, print, and log on to the network)											
sc config lmhosts start= auto	
PAUSE