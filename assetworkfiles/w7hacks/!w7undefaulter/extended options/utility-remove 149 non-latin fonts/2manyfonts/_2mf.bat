REM This is the batch file _2mf.bat version 1.0 from 2010-03-15
REM This batch file which will move 150 non-latin fonts
REM from the Windows\Fonts directory to a backup directory
REM Please read the readme.txt for more information
REM Latest version and news can be found in the blog-post
REM "How to remove Windows 7 non-latin fonts" on
REM http://www.support-ing.de/2010-02-11/how-to-remove-windows-7-not-latin-fonts
REM 
REM Use on your own risk
REM
REM Written by Support-Ing Martin Ihde, http://www.support-ing.de
REM 

@echo off
cls
set windrive=%1
echo Fonts will be moved from %windrive%:\Windows\Fonts\ to %windrive%:\2manyfonts\
set /p choice=Continue? Please press "y" for yes or "n" for no:  
if %choice%==y goto moveon
if %choice%==n goto dontmove

:moveon
REM take ownership
takeown /f %windrive%:\Windows\Fonts\aparaj.ttf
takeown /f %windrive%:\Windows\Fonts\aparajb.ttf
takeown /f %windrive%:\Windows\Fonts\aparajbi.ttf
takeown /f %windrive%:\Windows\Fonts\aparaji.ttf
takeown /f %windrive%:\Windows\Fonts\batang.ttc
takeown /f %windrive%:\Windows\Fonts\daunpenh.ttf
takeown /f %windrive%:\Windows\Fonts\dokchamp.ttf
takeown /f %windrive%:\Windows\Fonts\estre.ttf
takeown /f %windrive%:\Windows\Fonts\gautami.ttf
takeown /f %windrive%:\Windows\Fonts\gautamib.ttf
takeown /f %windrive%:\Windows\Fonts\gulim.ttc
takeown /f %windrive%:\Windows\Fonts\himalaya.ttf
takeown /f %windrive%:\Windows\Fonts\iskpota.ttf
takeown /f %windrive%:\Windows\Fonts\iskpotab.ttf
takeown /f %windrive%:\Windows\Fonts\kalinga.ttf
takeown /f %windrive%:\Windows\Fonts\kalingab.ttf
takeown /f %windrive%:\Windows\Fonts\kartika.ttf
takeown /f %windrive%:\Windows\Fonts\kartikab.ttf
takeown /f %windrive%:\Windows\Fonts\khmerui.ttf
takeown /f %windrive%:\Windows\Fonts\khmeruib.ttf
takeown /f %windrive%:\Windows\Fonts\kokila.ttf
takeown /f %windrive%:\Windows\Fonts\kokilab.ttf
takeown /f %windrive%:\Windows\Fonts\kokilabi.ttf
takeown /f %windrive%:\Windows\Fonts\kokilai.ttf
takeown /f %windrive%:\Windows\Fonts\Laoui.ttf
takeown /f %windrive%:\Windows\Fonts\Laouib.ttf
takeown /f %windrive%:\Windows\Fonts\latha.ttf
takeown /f %windrive%:\Windows\Fonts\lathab.ttf
takeown /f %windrive%:\Windows\Fonts\malgun.ttf
takeown /f %windrive%:\Windows\Fonts\malgunbd.ttf
takeown /f %windrive%:\Windows\Fonts\mangal.ttf
takeown /f %windrive%:\Windows\Fonts\mangalb.ttf
takeown /f %windrive%:\Windows\Fonts\meiryo.ttc
takeown /f %windrive%:\Windows\Fonts\meiryob.ttc
takeown /f %windrive%:\Windows\Fonts\micross.ttf
takeown /f %windrive%:\Windows\Fonts\mingliu.ttc
takeown /f %windrive%:\Windows\Fonts\mingliub.ttc
takeown /f %windrive%:\Windows\Fonts\monbaiti.ttf
takeown /f %windrive%:\Windows\Fonts\msgothic.ttc
takeown /f %windrive%:\Windows\Fonts\msjh.ttf
takeown /f %windrive%:\Windows\Fonts\msjhbd.ttf
takeown /f %windrive%:\Windows\Fonts\msmincho.ttc
takeown /f %windrive%:\Windows\Fonts\MSMINCHO.TTF
takeown /f %windrive%:\Windows\Fonts\msyh.ttf
takeown /f %windrive%:\Windows\Fonts\msyhbd.ttf
takeown /f %windrive%:\Windows\Fonts\msyi.ttf
takeown /f %windrive%:\Windows\Fonts\mvboli.ttf
takeown /f %windrive%:\Windows\Fonts\ntailu.ttf
takeown /f %windrive%:\Windows\Fonts\ntailub.ttf
takeown /f %windrive%:\Windows\Fonts\nyala.TTF
takeown /f %windrive%:\Windows\Fonts\phagspa.ttf
takeown /f %windrive%:\Windows\Fonts\phagspab.ttf
takeown /f %windrive%:\Windows\Fonts\plantc.ttf
takeown /f %windrive%:\Windows\Fonts\raavi.ttf
takeown /f %windrive%:\Windows\Fonts\raavib.ttf
takeown /f %windrive%:\Windows\Fonts\Shonar.ttf
takeown /f %windrive%:\Windows\Fonts\Shonarb.ttf
takeown /f %windrive%:\Windows\Fonts\shruti.ttf
takeown /f %windrive%:\Windows\Fonts\shrutib.ttf
takeown /f %windrive%:\Windows\Fonts\simsun.ttc
takeown /f %windrive%:\Windows\Fonts\simsunb.ttf
takeown /f %windrive%:\Windows\Fonts\taile.ttf
takeown /f %windrive%:\Windows\Fonts\taileb.ttf
takeown /f %windrive%:\Windows\Fonts\tunga.ttf
takeown /f %windrive%:\Windows\Fonts\tungab.ttf
takeown /f %windrive%:\Windows\Fonts\utsaah.ttf
takeown /f %windrive%:\Windows\Fonts\utsaahb.ttf
takeown /f %windrive%:\Windows\Fonts\utsaahbi.ttf
takeown /f %windrive%:\Windows\Fonts\utsaahi.ttf
takeown /f %windrive%:\Windows\Fonts\Vani.ttf
takeown /f %windrive%:\Windows\Fonts\Vanib.ttf
takeown /f %windrive%:\Windows\Fonts\vijaya.ttf
takeown /f %windrive%:\Windows\Fonts\vijayab.ttf
takeown /f %windrive%:\Windows\Fonts\vrinda.ttf
takeown /f %windrive%:\Windows\Fonts\vrindab.ttf

REM Change security
icacls %windrive%:\Windows\Fonts\aparaj.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\aparajb.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\aparajbi.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\aparaji.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\batang.ttc /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\daunpenh.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\dokchamp.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\estre.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\gautami.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\gautamib.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\gulim.ttc /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\himalaya.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\iskpota.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\iskpotab.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\kalinga.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\kalingab.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\kartika.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\kartikab.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\khmerui.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\khmeruib.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\kokila.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\kokilab.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\kokilabi.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\kokilai.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\Laoui.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\Laouib.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\latha.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\lathab.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\malgun.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\malgunbd.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\mangal.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\mangalb.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\meiryo.ttc /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\meiryob.ttc /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\micross.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\mingliu.ttc /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\mingliub.ttc /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\monbaiti.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\msgothic.ttc /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\msjh.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\msjhbd.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\msmincho.ttc /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\MSMINCHO.TTF /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\msyh.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\msyhbd.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\msyi.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\mvboli.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\ntailu.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\ntailub.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\nyala.TTF /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\phagspa.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\phagspab.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\plantc.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\raavi.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\raavib.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\Shonar.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\Shonarb.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\shruti.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\shrutib.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\simsun.ttc /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\simsunb.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\taile.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\taileb.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\tunga.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\tungab.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\utsaah.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\utsaahb.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\utsaahbi.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\utsaahi.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\Vani.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\Vanib.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\vijaya.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\vijayab.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\vrinda.ttf /grant administrators:F /t
icacls %windrive%:\Windows\Fonts\vrindab.ttf /grant administrators:F /t

REM moving font-files
move %windrive%:\Windows\Fonts\ahronbd.ttf %windrive%:\2manyfonts\ahronbd.ttf 
move %windrive%:\Windows\Fonts\andlso.ttf %windrive%:\2manyfonts\andlso.ttf
move %windrive%:\Windows\Fonts\angsa.ttf %windrive%:\2manyfonts\angsa.ttf
move %windrive%:\Windows\Fonts\angsab.ttf %windrive%:\2manyfonts\angsab.ttf
move %windrive%:\Windows\Fonts\angsai.ttf %windrive%:\2manyfonts\angsai.ttf
move %windrive%:\Windows\Fonts\angsau.ttf %windrive%:\2manyfonts\angsau.ttf
move %windrive%:\Windows\Fonts\angsaub.ttf %windrive%:\2manyfonts\angsaub.ttf
move %windrive%:\Windows\Fonts\angsaui.ttf %windrive%:\2manyfonts\angsaui.ttf
move %windrive%:\Windows\Fonts\angsauz.ttf %windrive%:\2manyfonts\angsauz.ttf
move %windrive%:\Windows\Fonts\angsaz.ttf %windrive%:\2manyfonts\angsaz.ttf
move %windrive%:\Windows\Fonts\aparaj.ttf %windrive%:\2manyfonts\aparaj.ttf
move %windrive%:\Windows\Fonts\aparajb.ttf %windrive%:\2manyfonts\aparajb.ttf
move %windrive%:\Windows\Fonts\aparajbi.ttf %windrive%:\2manyfonts\aparajbi.ttf
move %windrive%:\Windows\Fonts\aparaji.ttf %windrive%:\2manyfonts\aparaji.ttf
move %windrive%:\Windows\Fonts\arabtype.ttf %windrive%:\2manyfonts\arabtype.ttf
move %windrive%:\Windows\Fonts\batang.ttc %windrive%:\2manyfonts\batang.ttc
move %windrive%:\Windows\Fonts\browa.ttf %windrive%:\2manyfonts\browa.ttf
move %windrive%:\Windows\Fonts\browab.ttf %windrive%:\2manyfonts\browab.ttf
move %windrive%:\Windows\Fonts\browai.ttf %windrive%:\2manyfonts\browai.ttf
move %windrive%:\Windows\Fonts\browau.ttf %windrive%:\2manyfonts\browau.ttf
move %windrive%:\Windows\Fonts\browaub.ttf %windrive%:\2manyfonts\browaub.ttf
move %windrive%:\Windows\Fonts\browaui.ttf %windrive%:\2manyfonts\browaui.ttf
move %windrive%:\Windows\Fonts\browauz.ttf %windrive%:\2manyfonts\browauz.ttf
move %windrive%:\Windows\Fonts\browaz.ttf %windrive%:\2manyfonts\browaz.ttf
move %windrive%:\Windows\Fonts\cordia.ttf %windrive%:\2manyfonts\cordia.ttf
move %windrive%:\Windows\Fonts\cordiab.ttf %windrive%:\2manyfonts\cordiab.ttf
move %windrive%:\Windows\Fonts\cordiai.ttf %windrive%:\2manyfonts\cordiai.ttf
move %windrive%:\Windows\Fonts\cordiau.ttf %windrive%:\2manyfonts\cordiau.ttf
move %windrive%:\Windows\Fonts\cordiaub.ttf %windrive%:\2manyfonts\cordiaub.ttf
move %windrive%:\Windows\Fonts\cordiaui.ttf %windrive%:\2manyfonts\cordiaui.ttf
move %windrive%:\Windows\Fonts\cordiauz.ttf %windrive%:\2manyfonts\cordiauz.ttf
move %windrive%:\Windows\Fonts\cordiaz.ttf %windrive%:\2manyfonts\cordiaz.ttf
move %windrive%:\Windows\Fonts\daunpenh.ttf %windrive%:\2manyfonts\daunpenh.ttf
move %windrive%:\Windows\Fonts\david.ttf %windrive%:\2manyfonts\david.ttf
move %windrive%:\Windows\Fonts\davidbd.ttf %windrive%:\2manyfonts\davidbd.ttf
move %windrive%:\Windows\Fonts\dokchamp.ttf %windrive%:\2manyfonts\dokchamp.ttf
move %windrive%:\Windows\Fonts\estre.ttf %windrive%:\2manyfonts\estre.ttf
move %windrive%:\Windows\Fonts\frank.ttf %windrive%:\2manyfonts\frank.ttf
move %windrive%:\Windows\Fonts\gautami.ttf %windrive%:\2manyfonts\gautami.ttf
move %windrive%:\Windows\Fonts\gautamib.ttf %windrive%:\2manyfonts\gautamib.ttf
move %windrive%:\Windows\Fonts\gisha.ttf %windrive%:\2manyfonts\gisha.ttf
move %windrive%:\Windows\Fonts\gishabd.ttf %windrive%:\2manyfonts\gishabd.ttf
move %windrive%:\Windows\Fonts\gulim.ttc %windrive%:\2manyfonts\gulim.ttc
move %windrive%:\Windows\Fonts\himalaya.ttf %windrive%:\2manyfonts\himalaya.ttf
move %windrive%:\Windows\Fonts\iskpota.ttf %windrive%:\2manyfonts\iskpota.ttf
move %windrive%:\Windows\Fonts\iskpotab.ttf %windrive%:\2manyfonts\iskpotab.ttf
move %windrive%:\Windows\Fonts\kaiu.ttf %windrive%:\2manyfonts\kaiu.ttf
move %windrive%:\Windows\Fonts\kalinga.ttf %windrive%:\2manyfonts\kalinga.ttf
move %windrive%:\Windows\Fonts\kalingab.ttf %windrive%:\2manyfonts\kalingab.ttf
move %windrive%:\Windows\Fonts\kartika.ttf %windrive%:\2manyfonts\kartika.ttf
move %windrive%:\Windows\Fonts\kartikab.ttf %windrive%:\2manyfonts\kartikab.ttf
move %windrive%:\Windows\Fonts\KhmerUI.ttf %windrive%:\2manyfonts\KhmerUI.ttf
move %windrive%:\Windows\Fonts\KhmerUIb.ttf %windrive%:\2manyfonts\KhmerUIb.ttf
move %windrive%:\Windows\Fonts\kokila.ttf %windrive%:\2manyfonts\kokila.ttf
move %windrive%:\Windows\Fonts\kokilab.ttf %windrive%:\2manyfonts\kokilab.ttf
move %windrive%:\Windows\Fonts\kokilabi.ttf %windrive%:\2manyfonts\kokilabi.ttf
move %windrive%:\Windows\Fonts\kokilai.ttf %windrive%:\2manyfonts\kokilai.ttf
move %windrive%:\Windows\Fonts\LaoUI.ttf %windrive%:\2manyfonts\LaoUI.ttf
move %windrive%:\Windows\Fonts\LaoUIb.ttf %windrive%:\2manyfonts\LaoUIb.ttf
move %windrive%:\Windows\Fonts\latha.ttf %windrive%:\2manyfonts\latha.ttf
move %windrive%:\Windows\Fonts\lathab.ttf %windrive%:\2manyfonts\lathab.ttf
move %windrive%:\Windows\Fonts\leelawad.ttf %windrive%:\2manyfonts\leelawad.ttf
move %windrive%:\Windows\Fonts\leelawdb.ttf %windrive%:\2manyfonts\leelawdb.ttf
move %windrive%:\Windows\Fonts\lvnm.ttf %windrive%:\2manyfonts\lvnm.ttf
move %windrive%:\Windows\Fonts\lvnmbd.ttf %windrive%:\2manyfonts\lvnmbd.ttf
move %windrive%:\Windows\Fonts\majalla.ttf %windrive%:\2manyfonts\majalla.ttf
move %windrive%:\Windows\Fonts\majallab.ttf %windrive%:\2manyfonts\majallab.ttf
move %windrive%:\Windows\Fonts\malgun.ttf %windrive%:\2manyfonts\malgun.ttf
move %windrive%:\Windows\Fonts\malgunbd.ttf %windrive%:\2manyfonts\malgunbd.ttf
move %windrive%:\Windows\Fonts\mangal.ttf %windrive%:\2manyfonts\mangal.ttf
move %windrive%:\Windows\Fonts\mangalb.ttf %windrive%:\2manyfonts\mangalb.ttf
move %windrive%:\Windows\Fonts\meiryo.ttc %windrive%:\2manyfonts\meiryo.ttc
move %windrive%:\Windows\Fonts\meiryob.ttc %windrive%:\2manyfonts\meiryob.ttc
move %windrive%:\Windows\Fonts\micross.ttf %windrive%:\2manyfonts\micross.ttf
move %windrive%:\Windows\Fonts\mingliu.ttc %windrive%:\2manyfonts\mingliu.ttc
move %windrive%:\Windows\Fonts\mingliub.ttc %windrive%:\2manyfonts\mingliub.ttc
move %windrive%:\Windows\Fonts\monbaiti.ttf %windrive%:\2manyfonts\monbaiti.ttf
move %windrive%:\Windows\Fonts\moolbor.ttf %windrive%:\2manyfonts\moolbor.ttf
move %windrive%:\Windows\Fonts\mriam.ttf %windrive%:\2manyfonts\mriam.ttf
move %windrive%:\Windows\Fonts\mriamc.ttf %windrive%:\2manyfonts\mriamc.ttf
move %windrive%:\Windows\Fonts\msgothic.ttc %windrive%:\2manyfonts\msgothic.ttc
move %windrive%:\Windows\Fonts\msjh.ttf %windrive%:\2manyfonts\msjh.ttf
move %windrive%:\Windows\Fonts\msjhbd.ttf %windrive%:\2manyfonts\msjhbd.ttf
move %windrive%:\Windows\Fonts\msmincho.ttc %windrive%:\2manyfonts\msmincho.ttc
move %windrive%:\Windows\Fonts\msuighur.ttf %windrive%:\2manyfonts\msuighur.ttf
move %windrive%:\Windows\Fonts\msyh.ttf %windrive%:\2manyfonts\msyh.ttf
move %windrive%:\Windows\Fonts\msyhbd.ttf %windrive%:\2manyfonts\msyhbd.ttf
move %windrive%:\Windows\Fonts\msyi.ttf %windrive%:\2manyfonts\msyi.ttf
move %windrive%:\Windows\Fonts\mvboli.ttf %windrive%:\2manyfonts\mvboli.ttf
move %windrive%:\Windows\Fonts\nrkis.ttf %windrive%:\2manyfonts\nrkis.ttf
move %windrive%:\Windows\Fonts\ntailu.ttf %windrive%:\2manyfonts\ntailu.ttf
move %windrive%:\Windows\Fonts\ntailub.ttf %windrive%:\2manyfonts\ntailub.ttf
move %windrive%:\Windows\Fonts\nyala.ttf %windrive%:\2manyfonts\nyala.ttf
move %windrive%:\Windows\Fonts\phagspa.ttf %windrive%:\2manyfonts\phagspa.ttf
move %windrive%:\Windows\Fonts\phagspab.ttf %windrive%:\2manyfonts\phagspab.ttf
move %windrive%:\Windows\Fonts\plantc.ttf %windrive%:\2manyfonts\plantc.ttf
move %windrive%:\Windows\Fonts\raavi.ttf %windrive%:\2manyfonts\raavi.ttf
move %windrive%:\Windows\Fonts\raavib.ttf %windrive%:\2manyfonts\raavib.ttf
move %windrive%:\Windows\Fonts\rod.ttf %windrive%:\2manyfonts\rod.ttf
move %windrive%:\Windows\Fonts\Shonar.ttf %windrive%:\2manyfonts\Shonar.ttf
move %windrive%:\Windows\Fonts\Shonarb.ttf %windrive%:\2manyfonts\Shonarb.ttf
move %windrive%:\Windows\Fonts\shruti.ttf %windrive%:\2manyfonts\shruti.ttf
move %windrive%:\Windows\Fonts\shrutib.ttf %windrive%:\2manyfonts\shrutib.ttf
move %windrive%:\Windows\Fonts\simfang.ttf %windrive%:\2manyfonts\simfang.ttf
move %windrive%:\Windows\Fonts\simkai.ttf %windrive%:\2manyfonts\simkai.ttf
move %windrive%:\Windows\Fonts\simsun.ttc %windrive%:\2manyfonts\simsun.ttc
move %windrive%:\Windows\Fonts\simsunb.ttf %windrive%:\2manyfonts\simsunb.ttf
move %windrive%:\Windows\Fonts\taile.ttf %windrive%:\2manyfonts\taile.ttf
move %windrive%:\Windows\Fonts\taileb.ttf %windrive%:\2manyfonts\taileb.ttf
move %windrive%:\Windows\Fonts\tunga.ttf %windrive%:\2manyfonts\tunga.ttf
move %windrive%:\Windows\Fonts\tungab.ttf %windrive%:\2manyfonts\tungab.ttf
move %windrive%:\Windows\Fonts\upcdb.ttf %windrive%:\2manyfonts\upcdb.ttf
move %windrive%:\Windows\Fonts\upcdbi.ttf %windrive%:\2manyfonts\upcdbi.ttf
move %windrive%:\Windows\Fonts\upcdi.ttf %windrive%:\2manyfonts\upcdi.ttf
move %windrive%:\Windows\Fonts\upcdl.ttf %windrive%:\2manyfonts\upcdl.ttf
move %windrive%:\Windows\Fonts\upceb.ttf %windrive%:\2manyfonts\upceb.ttf
move %windrive%:\Windows\Fonts\upcebi.ttf %windrive%:\2manyfonts\upcebi.ttf
move %windrive%:\Windows\Fonts\upcei.ttf %windrive%:\2manyfonts\upcei.ttf
move %windrive%:\Windows\Fonts\upcel.ttf %windrive%:\2manyfonts\upcel.ttf
move %windrive%:\Windows\Fonts\upcfb.ttf %windrive%:\2manyfonts\upcfb.ttf
move %windrive%:\Windows\Fonts\upcfbi.ttf %windrive%:\2manyfonts\upcfbi.ttf
move %windrive%:\Windows\Fonts\upcfi.ttf %windrive%:\2manyfonts\upcfi.ttf
move %windrive%:\Windows\Fonts\upcfl.ttf %windrive%:\2manyfonts\upcfl.ttf
move %windrive%:\Windows\Fonts\upcib.ttf %windrive%:\2manyfonts\upcib.ttf
move %windrive%:\Windows\Fonts\upcibi.ttf %windrive%:\2manyfonts\upcibi.ttf
move %windrive%:\Windows\Fonts\upcii.ttf %windrive%:\2manyfonts\upcii.ttf
move %windrive%:\Windows\Fonts\upcil.ttf %windrive%:\2manyfonts\upcil.ttf
move %windrive%:\Windows\Fonts\upcjb.ttf %windrive%:\2manyfonts\upcjb.ttf
move %windrive%:\Windows\Fonts\upcjbi.ttf %windrive%:\2manyfonts\upcjbi.ttf
move %windrive%:\Windows\Fonts\upcji.ttf %windrive%:\2manyfonts\upcji.ttf
move %windrive%:\Windows\Fonts\upcjl.ttf %windrive%:\2manyfonts\upcjl.ttf
move %windrive%:\Windows\Fonts\upckb.ttf %windrive%:\2manyfonts\upckb.ttf
move %windrive%:\Windows\Fonts\upckbi.ttf %windrive%:\2manyfonts\upckbi.ttf
move %windrive%:\Windows\Fonts\upcki.ttf %windrive%:\2manyfonts\upcki.ttf
move %windrive%:\Windows\Fonts\upckl.ttf %windrive%:\2manyfonts\upckl.ttf
move %windrive%:\Windows\Fonts\upclb.ttf %windrive%:\2manyfonts\upclb.ttf
move %windrive%:\Windows\Fonts\upclbi.ttf %windrive%:\2manyfonts\upclbi.ttf
move %windrive%:\Windows\Fonts\upcli.ttf %windrive%:\2manyfonts\upcli.ttf
move %windrive%:\Windows\Fonts\upcll.ttf %windrive%:\2manyfonts\upcll.ttf
move %windrive%:\Windows\Fonts\utsaah.ttf %windrive%:\2manyfonts\utsaah.ttf
move %windrive%:\Windows\Fonts\utsaahb.ttf %windrive%:\2manyfonts\utsaahb.ttf
move %windrive%:\Windows\Fonts\utsaahbi.ttf %windrive%:\2manyfonts\utsaahbi.ttf
move %windrive%:\Windows\Fonts\utsaahi.ttf %windrive%:\2manyfonts\utsaahi.ttf
move %windrive%:\Windows\Fonts\Vani.ttf %windrive%:\2manyfonts\Vani.ttf
move %windrive%:\Windows\Fonts\Vanib.ttf %windrive%:\2manyfonts\Vanib.ttf
move %windrive%:\Windows\Fonts\vijaya.ttf %windrive%:\2manyfonts\vijaya.ttf
move %windrive%:\Windows\Fonts\vijayab.ttf %windrive%:\2manyfonts\vijayab.ttf
move %windrive%:\Windows\Fonts\vrinda.ttf %windrive%:\2manyfonts\vrinda.ttf
move %windrive%:\Windows\Fonts\vrindab.ttf %windrive%:\2manyfonts\vrindab.ttf
echo . 
echo . 
echo Finished!  :-)
echo .  
pause
exit /B n


:dontmove
echo .   
echo Batch file execution canceled
pause
