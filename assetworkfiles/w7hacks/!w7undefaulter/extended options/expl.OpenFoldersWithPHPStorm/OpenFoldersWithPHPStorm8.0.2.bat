@echo off
 
SET PhpStormPath=C:\Program Files (x86)\JetBrains\PhpStorm 8.0.2\bin\PhpStorm64.exe

echo Adding folder entries
@reg add "HKEY_CLASSES_ROOT\Folder\shell\Open folder in PhpStorm" /t REG_SZ /v "" /d "Open folder in PhpStorm" /f
@reg add "HKEY_CLASSES_ROOT\Folder\shell\Open folder in PhpStorm" /t REG_EXPAND_SZ /v "Icon" /d "%PhpStormPath%,0" /f
@reg add "HKEY_CLASSES_ROOT\Folder\shell\Open folder in PhpStorm\command" /t REG_SZ /v "" /d "%PhpStormPath% \"%%1\"" /f
 
pause 