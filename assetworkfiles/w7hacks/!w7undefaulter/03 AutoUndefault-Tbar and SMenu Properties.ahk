Hotkey, End, EarlyTerm
Send {LWin}
SendRaw taskbar and start menu
Send {Enter}
WinWait Taskbar and Start Menu Properties
WinActivate
Sleep 80
Send {Tab 4}{Sleep 40}
Send {Down}{Sleep 60}   ;set: combine when full
Send {Tab}{Sleep 60}
Send {Space}{Sleep 60} ;notif customize launched...
WinWait Notification Area Icons
WinActivate
Send +{Tab}{Sleep 40}
Send {Space}{Sleep 40}
Send {Tab}{Sleep 40}
Send {Space}{Sleep 40}
WinWait Taskbar and Start Menu Properties
WinActivate
Send {Shift Down}
Send {Tab}{Sleep 10}{Tab}{Sleep 10}{Tab}{Sleep 10}
Send {Tab}{Sleep 10}{Tab}{Sleep 10}{Space}{Sleep 10}{Tab}{Sleep 10}
Send {Shift Up}{Sleep 20}
Send {Right}{Sleep 30}
Send {Tab}
Send {Space}
WinWait Customize Start Menu
WinActivate
Send {Down 4}{Sleep 10}
Send {Space} ; disable connect to
Send {Down 3}{Sleep 10}
Send {Space} ; ctrl panel as menu
Send {Down 2}{Sleep 10}
Send {Space} ; default programs off
Send {Down}{Sleep 10}
Send {Space} ; devices off
Send {Down 2}{Sleep 10}
Send {Space} ; documents as link
Send {Down 4}{Sleep 10}
Send {Space} ; downloads as link
Send {Down 9}{Sleep 10}
Send {Space} ; help off
Send {Down}{Sleep 10}
Send {Space} ; highlight off
Send {Down 5}{Sleep 10}
Send {Space} ; music off
Send {Down 10}{Sleep 10}
Send {Space} ; pictures off
Send {Down}{Sleep 10}
Send {Space} ; recent items on
Send {Down 16}{Sleep 10}
Send {Space} ; use small icons
Send {Tab 4}{Sleep 10}
Send {Space}{Sleep 30} ;exit customize
Send {Tab 5}
Send {Enter} ; exit main menu


EarlyTerm:
ExitApp