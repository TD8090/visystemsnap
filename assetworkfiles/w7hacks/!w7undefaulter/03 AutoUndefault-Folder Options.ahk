Hotkey, End, EarlyTerm
Send {LWin}
SendRaw folder options
Send {Enter}
WinWait Folder Options
WinActivate
Sleep 30
Send {Tab 2}{Sleep 10}
Send {Space}{Sleep 10}   ; show all folders
Send {Down}{Sleep 10}   ;
Send {Space}{Sleep 20}   ; auto expand
Send +{Tab 4}{Sleep 10}
Send {Right}{Sleep 10}  ; view tab
Send +{Tab 5}{Sleep 30}
;Send {Down 8}{Sleep 30}
;Send {Space}{Sleep 30} ; show hidden files
Send {Down 10}{Sleep 30}
Send {Space}{Sleep 30} ; show file extensions
Send {Tab 2}{Sleep 20}{Enter}


EarlyTerm:
ExitApp