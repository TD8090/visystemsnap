Hotkey, End, EarlyTerm

;DEFINE permChg
permChg(){
Send !ep														
WinWait Permissions for ShellFolder											
WinActivate Permissions for ShellFolder										
Send {Down}{Sleep 5}{Tab}{Sleep 5}{Tab}{Sleep 5}{Tab}{Sleep 5}{Space}{Sleep 5}{Enter}	
return
}

;DEFINE mainFunc
mainFunc(path){
clipboard = %path%
ClipWait 
Run, RegJump.exe
WinWait Registry Editor
WinActivate
permChg()
WinActivate Registry Editor
WinClose Registry Editor
Sleep 30
return
}
mainFunc("HKEY_CLASSES_ROOT\CLSID\{031E4825-7B94-4dc3-B131-E946B44C8DD5}\ShellFolder")
mainFunc("HKEY_CLASSES_ROOT\CLSID\{B4FB3F98-C1EA-428d-A78A-D1F5659CBA93}\ShellFolder")
mainFunc("HKEY_CLASSES_ROOT\CLSID\{F02C1A0D-BE21-4350-88B0-7367FC96EF3C}\ShellFolder")
mainFunc("HKEY_CLASSES_ROOT\Wow6432Node\CLSID\{031E4825-7B94-4dc3-B131-E946B44C8DD5}\ShellFolder")


EarlyTerm:
ExitApp
