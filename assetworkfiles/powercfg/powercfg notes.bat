:: Powercfg -setactive 8c5e7fda-e8bf-4a96-9a85-a6e23a8c635c
:: systempropertiesperformance

:: require password on wakeup "No"
:: POWERCFG -SETACVALUEINDEX 8c5e7fda-e8bf-4a96-9a85-a6e23a8c635c fea3413e-7e05-4911-9a71-700331f1c294 0e796bdb-100d-47d6-a2d5-f7d2daa51f51 0
:: POWERCFG -SETDCVALUEINDEX 8c5e7fda-e8bf-4a96-9a85-a6e23a8c635c fea3413e-7e05-4911-9a71-700331f1c294 0e796bdb-100d-47d6-a2d5-f7d2daa51f51 0

::===============================
::CREATE / SELECT A POWER SCHEME
::===============================
:: Balanced, High Performance, and Power Saver
::--these 3 default schemes have the same GUID on every Win7 

::1) create new power scheme based on existing
:: 		if newguid is omitted, it will autogenerate a guid
:: POWERCFG -DUPLICATESCHEME <POWER SCHEME GUID> <NEW GUID>

::2) list available schemes, mark active with *
:: c:\> powercfg /L

:: Existing Power Schemes (* Active)
:: -----------------------------------
:: Power Scheme GUID: 381b4222-f694-41f0-9685-ff5bb260df2e  (Balanced) *
:: Power Scheme GUID: 8c5e7fda-e8bf-4a96-9a85-a6e23a8c635c  (High performance)
:: Power Scheme GUID: a1841308-3541-4fab-bc81-f71556f20b4a  (Power saver)

::3) Set a scheme as active
::set "High Performance" as active
:: c:\> powercfg /S 8c5e7fda-e8bf-4a96-9a85-a6e23a8c635c 

::Power Scheme GUID: 9d7849b6-0359-4a43-97ae-7e4cf5afb9c3  (VI Power Scheme) *
