#NoEnv
SendMode Input
CoordMode, Mouse, Screen
#Include includes/incl_ini.ahk
#Include includes/incl_WinMagicMove.ahk
#include includes/incl_Fonts.ahk
#Include includes/incl_ImageButton.ahk
SetWorkingDir %A_ScriptDir%
; MsgBox [, Options, Title, Text, Timeout]
bminiPath := "bm_settings.ini"
visiniPath := "C:\Users\" A_UserName "\VIPSystem\settings.ini"
IfNotExist, %bminiPath%
		CreateConfigFile(bminiPath)
ViSystemFolder = no
ViSystemFile   = no
LocateVISystem(ViSysFolder, ViSysFile)

FileRead, bmini, %bminiPath%
Location1 := ini_getValue(bmini, "UserVars", "Location1")
Location2 := ini_getValue(bmini, "UserVars", "Location2")
Location3 := ini_getValue(bmini, "UserVars", "Location3")
Location4 := ini_getValue(bmini, "UserVars", "Location4")
BomCity1 := ini_getValue(bmini, "UserVars", "BomCity1")
BomCity2 := ini_getValue(bmini, "UserVars", "BomCity2")
BomCity3 := ini_getValue(bmini, "UserVars", "BomCity3")
BomCity4 := ini_getValue(bmini, "UserVars", "BomCity4")
PcName1 := ini_getValue(bmini, "UserVars", "PcName1")
PcName2 := ini_getValue(bmini, "UserVars", "PcName2")
PcName3 := ini_getValue(bmini, "UserVars", "PcName3")
PcName4 := ini_getValue(bmini, "UserVars", "PcName4")
BomLogin := ini_getValue(bmini, "UserVars", "BomLogin")
BomPassw := ini_getValue(bmini, "UserVars", "BomPassword")
LocChoices := Location1 "|" Location2 "|" Location3 "|" Location4
BomCityChoices := BomCity1 "|" BomCity2 "|" BomCity3 "|" BomCity4
PcNameString := PcName1 "|" PcName2 "|" PcName3 "|" PcName4
LocCheckString := A_UserName "\VIPSystem ( " ViSysFolder " ) \settings.ini ( " ViSysFile " )"
ChosenTimezone = Tz1_East

; LastDriveSerial := ini_getValue(bmini, "UserVars", "LastDriveSerial")

ihashes := ini_getAllKeyNames(bmini, "CommonHashes")
imachesA := ini_getValue(bmini, "UserVars", "MachArrayA")
imachesB := ini_getValue(bmini, "UserVars", "MachArrayB")
hashArray := StrSplit(ihashes, ",")
machArrayA := StrSplit(imachesA, ",")
machArrayB := StrSplit(imachesB, ",")
dfltHash := ini_getValue(bmini, "UserVars", "Hash")
dfltMach := ini_getValue(bmini, "UserVars", "Machine")
finalHashRad := dfltHash
finalMachRad := dfltMach

objWMIService := ComObjGet("winmgmts:{impersonationLevel=impersonate}!\\" . A_ComputerName . "\root\cimv2")
GetSysName(){
	global
	colSYS := objWMIService.ExecQuery("SELECT Name, BootupState FROM Win32_ComputerSystem")._NewEnum
	While colSYS[objSYS] { 
		SysName := objSYS.Name		
		; msgbox,,,% objSYS.Name		
	}
}
GetSysName()
colDRIVE := objWMIService.ExecQuery("SELECT SerialNumber FROM Win32_DiskDrive")._NewEnum
While colDRIVE[objDRIVE] { 
	DriveSerial := objDRIVE.SerialNumber
}
; msgbox,,, % "`n" DriveSerial "`n" PCname "`n" BIOSver "`n" BIOSverver "`n" SYSname "`n" SYSdesc "`n" BIOSdesc  ,1




; GUI ================== GUI ======================== GUI ======================= GUI ===================== GUI ===================== GUI
; GUI ================== GUI ======================== GUI ======================= GUI ===================== GUI ===================== GUI

; +0x02000000 = WS_CLIPCHILDREN, prevents parent window from redrawing over child window
; +0x4000000 = WS_CLIPSIBLINGS  Clips child windows relative to each other
; LastFound activates our new window. required for MagicMove and maybe more
Gui, +LastFound +AlwaysOnTop -Caption +hwnd_ViSystemSnap
Gui, Add, Picture, x404 y168, assets/VSSTitle.png

test.p
test.t1

; font.p
gcolor.dkgrey
AddCtrls_ViSettings()
AddCtrls_PcName()
AddCtrls_Timezone()
AddCtrls_GetStatus()
AddCtrls_Bomgar()
AddCtrls_Openers()

;<<PICTURE>> BACKGROUND 
; Gui, Add, Picture, x0 y0 w420 h300 +0x4000000, assets/VIbg.png
Gui, Add, Button, cOrange x400 y0 w20 h20 gGoExitApp hwnd_BtnCloseX, X
EXOpt1 := [7, 0xE5B300,   0xFF000000, 0xCC7700, , 0x555555, "Gray", 1]
EXOpt2 := [7, 0xE5B300,   0x555555, "Black", , 0x555555, "Gray", 1]
EXOpt3 := [7, 0xFF000000, 0x654D1E, "White", , 0x555555, "Gray", 1]
ImageButton.Create(_BtnCloseX, EXOpt1, EXOpt2, EXOpt3)

; font.p
; font.h1
GuiControl, Font, %_H1loc%
GuiControl, Font, %_H1mach%
GuiControl, Font, %_H1hash%
GuiControl, Font, %_H1pcname%
GuiControl, Font, %_H1tzone%
GuiControl, Font, %_H1status%
; font.h2
GuiControl, Font, %_H1bom%
; font.code
GuiControl, Font, %_NameChoice%
GuiControl, Font, %_BOMLOG%
GuiControl, Font, %_BOMPAS%
; font.psmall
GuiControl, Font, %_OPENADMI%
GuiControl, Font, %_OPENPF86%
GuiControl, Font, %_OPENINST%

Gui, Show, Hide
WinMagicMove()
Gui, Show, w420 h300, ViSystemSnap
Gui, Add, Picture, w420 h300 x0 y0 +BackgroundTrans gUiMove
HideFocusBorder(_ViSystemSnap)

; ExitApp
; /GUI ================== /GUI ======================== /GUI ======================= /GUI ===================== /GUI ===================== /GUI
; /GUI ================== /GUI ======================== /GUI ======================= /GUI ===================== /GUI ===================== /GUI

; -- END AUTOEXECUTE
return
; -- END AUTOEXECUTE


AddCtrls_ViSettings(){ ;SETTINGS
	global
	Gui, Add, Text, 		w240 h13 x0  y0   hwnd_LocCheck, %LocCheckString%
	Gui, Add, Text, 		w65  h12 x0  y16  hwnd_H1loc Center, Location
	Gui, Add, ComboBox, w140 h80 x0  y30  vLocChoice Simple Choose1, %LocChoices%
	Gui, Add, Text, 		w75  h12 x0  y108 hwnd_H1mach, Machine
	MakeRadiosFromArray(machArrayA, "w20 h15 x7", 122, 14, "", "gGoRadMaches", dfltMach) 
	MakeRadiosFromArray(machArrayB, "w20 h15 x27", 122, 14, "", "gGoRadMaches", dfltMach) 
	Gui, Add, Text, 		w72  h12 x68 y108 hwnd_H1hash, Hashes
	MakeRadiosFromArray(hashArray, "w90 h15 x50", 122, 14, "", "gGoRadHashes", dfltHash) 
	Gui, Add, Button, 	w90  h15 x0  y236 hwnd_LauSet gLaunchSettings, Write settings
	BlueB1 := [3, 0xaec9d7, 0x608fa7, "Black", 5, 0x555555]
	BlueB2 := [3, 0xDD0000, 0x550000, "White", 5, 0x555555]
	BlueB3 := [3, 0x608fa7, 0xaec9d7, "White", 5, 0x555555]
	ImageButton.Create(_LauSet, BlueB1, BlueB2, BlueB3)

	Gui, Add, Text, c0xc4e5f6 w140 h13 x0 yp+16 hwnd_ViSetReport, ---
}
AddCtrls_PcName(){ ;PCNAME
	global
	Gui, Add, Text, x233 y16 h13 w160 hwnd_PcCheck, %SYSname%
	Gui, Add, Text, 		w65  h12 x150 y16 hwnd_H1pcname, PC Name
	Gui, Add, Text, 		w15  h12 x215 y16 hwnd_NAMELEN, --
	Gui, Add, ComboBox, w112 h85 x150 y30 vNameChoice hwnd_NameChoice Simple Choose1, %PcNameString%
	Gui, Add, Button, 	w100 h15 x150 y114 gLaunchSysName hwnd_LauPcNm, Change PC Name
	ImageButton.Create(_LauPcNm, BlueB1, BlueB2, BlueB3)

}
AddCtrls_Timezone(){ ;TIMEZONE
	global
	Gui, Add, Text,   w65  h15 x150 y133  hwnd_H1tzone, Timezone
	Gui, Add, Radio,  w95  hp   xp   yp+15 gGoTimezone Checked, Tz1_East 	
	Gui, Add, Radio,  w95  hp   xp   yp+15 gGoTimezone, Tz2_Indiana 
	Gui, Add, Radio,  w95  hp   xp   yp+15 gGoTimezone, Tz3_Central 
	Gui, Add, Radio,  w95  hp   xp   yp+15 gGoTimezone, Tz4_Mountain
	Gui, Add, Radio,  w95  hp   xp   yp+15 gGoTimezone, Tz5_Arizona 
	Gui, Add, Radio,  w95  hp   xp   yp+15 gGoTimezone, Tz6_Pacific 
	Gui, Add, Button, w75  hp xp   yp+15  +0x1000 gLaunchTimezone hwnd_LauTz, Set TimeZone
	ImageButton.Create(_LauTz, BlueB1, BlueB2, BlueB3)

	Gui, Add, Text, c0xc4e5f6 w130 h13 xp yp+16 hwnd_tzreport, ----
}
AddCtrls_GetStatus(){ ;GET STATUS
	global
	Gui, Add, Button, w150 h102 x265 y30 Disabled hwnd_STATUSBackGround, 
	GSbg := [4, 0xFF000000, 0x555555, "Black", 9, 0x555555, 0x4a1d00, 2]
	ImageButton.Create(_STATUSBackGround, GSbg, GSbg)
	Gui, Add, Button, w120 h15 xp+15 yp+3 hwnd_LauStat gLaunchGetStatus, Get Status
	Grey1 := [0, 0x4a1d00,, 0xcc7700, 0, 0x555555, 0xcc7700, 1]
	Grey2 := [0, 0xfd6100,, "White", 0, 0x555555, 0xcc7700, 1]
	Grey3 := [0, 0x555555,, "White", 0, 0x555555, 0xcc7700, 1]
	ImageButton.Create(_LauStat,Grey1,Grey2,Grey3)

	Gui, Add, Text, c0xfd6100  w12  h15  xp-12 yp+18  +BackgroundTrans hwnd_VIL, L:
	Gui, Add, Text, c0xfd6100  w12  h15  xp    yp+16  +BackgroundTrans hwnd_VIM, M:
	Gui, Add, Text, c0xfd6100  w12  h15  xp    yp+16  +BackgroundTrans hwnd_VIP, P:
	Gui, Add, Text, c0xfd6100  w12  h15  xp    yp+16  +BackgroundTrans hwnd_VIH, H:
	Gui, Add, Text, c0xfd6100  w52  h15  xp    yp+16  +BackgroundTrans hwnd_VIB, #Boms:
	Gui, Add, Text, c0xCB8300  w134 h15  xp+12 yp-64  +BackgroundTrans hwnd_VILOC , --
	Gui, Add, Text, c0xCB8300  w134 h15  xp    yp+16  +BackgroundTrans hwnd_VIMACH, --
	Gui, Add, Text, c0xCB8300  w134 h15  xp    yp+16  +BackgroundTrans hwnd_VIPASS, --
	Gui, Add, Text, c0xCB8300  w134 h15  xp    yp+16  +BackgroundTrans hwnd_VIHASH, --
	Gui, Add, Text, c0xCB8300  w60  h15  xp+26 yp+16  +BackgroundTrans hwnd_VIBOMN, --
}
AddCtrls_Openers(){	;OPENERS
	global 
	Gui, Add, Button,   w40 h12 x2   y272 hwnd_OPENADMI gOpenAdmin 	  , Admin
	Gui, Add, Button,   w40 h12 x2   y286 hwnd_OPENPF86 gOpenPF86 		, PF86
	Gui, Add, Button,   w60 h12 x50  y272 hwnd_OPENAPPW gOpenAppwiz , Appwiz
	Gui, Add, Button,   w60 h12 x50  y286 hwnd_OPENINST gOpenInstaller, Installer
	Gui, Add, Button,   w60 h12 x120 y272 hwnd_OPENNONO gOpenNoNotif, NoNotif
	Gui, Add, Button,   w60 h12 x120 y286 hwnd_OPENFAVP gOpenFavPins, FavPins
	Gui, Add, Button,   w60 h12 x190 y272 hwnd_OPENBCDE gOpenBCDEdits, BCDEdit
	Gui, Add, Button,   w60 h12 x190 y286 hwnd_OPENPOWR gOpenPowerPlan, PowerPlan
	OpOpt1 := [3, 0xCC7700, 0xffa82e, "Black", 5, 0x555555]
	OpOpt2 := [3, 0xDD0000, 0x550000, "White", 5, 0x555555]
	OpOpt3 := [3, 0xffa82e, 0xCC7700, "White", 5, 0x555555]
	ImageButton.Create(_OPENADMI, OpOpt1, OpOpt2, OpOpt3)
	ImageButton.Create(_OPENPF86, OpOpt1, OpOpt2, OpOpt3)
	ImageButton.Create(_OPENINST, OpOpt1, OpOpt2, OpOpt3)
	ImageButton.Create(_OPENAPPW, OpOpt1, OpOpt2, OpOpt3)
	ImageButton.Create(_OPENNONO, OpOpt1, OpOpt2, OpOpt3)
	ImageButton.Create(_OPENFAVP, OpOpt1, OpOpt2, OpOpt3)
	ImageButton.Create(_OPENBCDE, OpOpt1, OpOpt2, OpOpt3)
	ImageButton.Create(_OPENPOWR, OpOpt1, OpOpt2, OpOpt3)

}
AddCtrls_Bomgar(){	;BOMGAR 
	global
	Gui, Add, Button, w120 h140 x280 y140 Disabled hwnd_BOMBackGround, 
	BMbg := [0, 0xCC7700, 0x654D1E, "Black", 6, 0x555555]
	ImageButton.Create(_BOMBackGround, BMbg, BMbg)
	Gui, Add, Text,     wp h12 xp yp  hwnd_H1bom Center +BackgroundTrans, % "B o m - M a k e r"
	Gui, Add, ComboBox, wp h80 xp yp+13 vCityChoice Simple Choose1, %BomCityChoices%
	Gui, Add, Button,   wp h15 xp yp+80 gLaunchBommaker hwnd_BtnMakeBom, Make Bomgar
	BomOpt1 := [3, 0xCC7700, 0xffa82e, "Black", 5, 0xCC7700]
	BomOpt2 := [3, 0xDD0000, 0x550000, "White", 5, 0xCC7700]
	BomOpt3 := [3, 0xffa82e, 0xCC7700, "White", 5, 0xCC7700]
	ImageButton.Create(_BtnMakeBom, BomOpt1, BomOpt2, BomOpt3)
	Gui, Add, Text,     wp h14 x281 yp+17 hwnd_BOMLOG Center +BackgroundTrans, %BomLogin%
	Gui, Add, Text,     wp h14 x281 yp+15 hwnd_BOMPAS Center +BackgroundTrans, %BomPassw%
}

UiMove:
{
	PostMessage, 0xA1, 2,,, A 
	Return
}
OpenAdmin:
{
	if(ViSysFolder = "yes"){
		IfWinExist, ahk_exe explorer.exe, C:\Users\%A_UserName%\VIPSystem
		{
			WinActivate, ahk_exe explorer.exe, C:\Users\%A_UserName%\VIPSystem
		}
		else
		{
			Run, explore C:\Users\%A_UserName%\VIPSystem
			WinWait, ahk_exe explorer.exe, C:\Users\%A_UserName%\VIPSystem
		}	
		WinMove,, C:\Users\%A_UserName%\VIPSystem, 70, 150, 600, 500
	}	
	else{
		IfWinExist, ahk_exe explorer.exe, C:\Users\%A_UserName%
		{
			WinActivate, ahk_exe explorer.exe, C:\Users\%A_UserName%
		}
		else
		{
			Run, explore C:\Users\%A_UserName%\VIPSystem
			WinWait, ahk_exe explorer.exe, C:\Users\%A_UserName%
		}	
		WinMove,, C:\Users\%A_UserName%\VIPSystem, 70, 150, 600, 500
	}

	return
}
OpenPF86:
{
	IfWinExist, ahk_exe explorer.exe, C:\Program Files (x86)
	{
		WinActivate, ahk_exe explorer.exe, C:\Program Files (x86)
	}
	else
	{
		Run, explore C:\Program Files (x86)\
		WinWait, ahk_exe explorer.exe, C:\Program Files (x86)
	}	
	WinMove,, Program Files (x86), 70, 550, 600, 500
	return
}
OpenAppwiz:
{
	IfWinExist, ahk_exe explorer.exe, Programs and Features
	{
		WinActivate, ahk_exe explorer.exe, Programs and Features
	}
	else
	{
		Send {LWin}
		Sleep 50
		Send appwiz.cpl
		Sleep 50
		Send {Control Down}{Shift Down}{Enter}{Shift Up}{Control Up}
		WinWait, ahk_exe explorer.exe, Programs and Features
	}	
	WinMove,ahk_exe explorer.exe, Programs and Features, 330, 20, 550, 750


	; Run %comspec% /c "control appwiz.cpl"
	return
}

OpenNoNotif:
{
	Run, "assets\VSSNoNotif.reg"
	WinWait, ahk_exe regedit.exe, &Yes
	WinActivate, ahk_exe regedit.exe, &Yes
	Sleep 200
	Send, {Enter}
	Sleep 500
	Send, {Enter}
	return
}
OpenFavPins:
{
	Run, "assets\VSSFavPins.vbs",,, pid	
	return
}
OpenPowerPlan:
{
	Gui, +LastFound -AlwaysOnTop
	Run, "assets\VSSPowerScheme.bat",,, pplpid
	WinWait ahk_pid %pplpid%
	WinActivate ahk_pid %pplpid%
	WinMove,,, 0, (A_ScreenHeight - 311), 653, 311
	Gui, +LastFound +AlwaysOnTop
	return
}
OpenBCDEdits:
{
	Gui, +LastFound -AlwaysOnTop
	Run, "assets\VSSBCDEdits.bat",,, bcdpid
	WinWait ahk_pid %bcdpid%
	WinActivate ahk_pid %bcdpid%
	WinMove,,, 0, (A_ScreenHeight - 454), 381, 143
	Gui, +LastFound +AlwaysOnTop
	return
}
OpenInstaller:
{
	Run, "assets\VIPInstaller.exe"
	return
}
LaunchGetStatus:
{
	FileRead, visini, %visiniPath%
	currvLoc := ini_getValue(visini, "Client", "Location")
	currvMach := ini_getValue(visini, "Client", "Machine")
	currvHash := ini_getValue(visini, "Client", "Password")
	for cvidx, cvelm in hashArray
	{
		cvthisval := ini_getValue(bmini, "CommonHashes", cvelm)
		if(currvHash = cvthisval){
				currvPass := cvelm
		}
	}
	if(!currvPass){
				currvPass := "unknown..."
	}	
	currboms := 0
	Loop, Files, C:\Users\All Users\bomgar*, D
	{
		currboms := A_Index
	}	
	GuiControl, Text, %_VIL%, L:
	GuiControl, Text, %_VIM%, M:
	GuiControl, Text, %_VIP%, P:
	GuiControl, Text, %_VIH%, H:
	GuiControl, Text, %_VIB%, #Boms:
	GuiControl, Text, %_VILOC%,  %currvLoc%	
	GuiControl, Text, %_VIMACH%, %currvMach%	
	GuiControl, Text, %_VIHASH%, %currvHash%	
	GuiControl, Text, %_VIPASS%, %currvPass%	
	GuiControl, Text, %_VIBOMN%, %currboms%		
	Run %comspec% /c "control system"
	WinWait, System	
	WinActivate, System
	WinMove, System, , 25, 25, 750, 310
	Send {Down}{Down}{Down}{Down}
	return
}

GoRadHashes:
{	
	finalHashRad = %A_GuiControl%
	return
}
GoRadMaches:
{
	finalMachRad = %A_GuiControl%
	return
}
GoTimezone:
{
	ChosenTimezone = %A_GuiControl%
	return
}

LaunchTimezone:
{
	Gui, Submit, NoHide
	if (ChosenTimezone = "Tz1_East"){ 
		zone = Eastern Standard Time 
	}
	if (ChosenTimezone = "Tz2_Indiana"){ 
		zone = US Eastern Standard Time 
	}
	if (ChosenTimezone = "Tz3_Central"){ 
		zone = Central Standard Time 
	}
	if (ChosenTimezone = "Tz4_Mountain"){ 
		zone = Mountain Standard Time 
	}
	if (ChosenTimezone = "Tz5_Arizona"){ 
		zone = US Mountain Standard Time 
	}
	if (ChosenTimezone = "Tz6_Pacific"){ 
		zone = Pacific Standard Time 
	}
	; msgbox,,, % "ChosenTimezone: " ChosenTimezone "`nzone: " zone  ,2	
	commandstring = tzutil /s `"%zone%`"
	Run %comspec% /c %commandstring%
	GuiControl, Text, %_tzreport%, % ">" . zone
	return	
} ;//end//LaunchTimezone

LaunchSysName:
{
	Gui, Submit, NoHide
	namechars := StrLen(NameChoice)
	if(namechars < 16){
		gui,font, c0xc4e5f6 s10 bold, Courier New
		GuiControl, Font, %_NameLen%
		GuiControl, Text, %_NameLen%, %namechars%	
		Gui, Font, cBlack s8 norm, Tahoma

		RegExMatch(NameChoice, "(.*)(\d(?!.*\d))(.*)", Pa)
		NewName := Pa1  (Pa2+1)  Pa3

		if(NameChoice){
			if (NameChoice != PcName1){	; we didn't accept increment
				if (NameChoice = PcName2){ ; we used previous name
					PcName4 := PcName4 
					PcName3 := PcName3 	
					PcName2 := NameChoice 
					PcName1 := NewName	
				}
				else if (NameChoice = PcName3){
					PcName4 := PcName4 
					PcName3 := PcName1 	
					PcName2 := NameChoice 
					PcName1 := NewName	
				}
				else if (NameChoice = PcName4){
					PcName4 := PcName3 
					PcName3 := PcName1 
					PcName2 := NameChoice 
					PcName1 := NewName	
				}
				else { ;completely unique
					PcName4 := PcName3 
					PcName3 := PcName1 	
					PcName2 := NameChoice 
					PcName1 := NewName	
				}		
			}	
			else { ;we accepted increment
				PcName4 := PcName4 			
				PcName3 := PcName3 	
				PcName2 := NameChoice 
				PcName1 := NewName	
			}
			ini_replaceValue(bmini, "UserVars", "PcName1", PcName1)
			ini_replaceValue(bmini, "UserVars", "PcName2", PcName2)
			ini_replaceValue(bmini, "UserVars", "PcName3", PcName3)
			ini_replaceValue(bmini, "UserVars", "PcName4", PcName4)
			UpdateConfigFile(bminiPath, bmini)
			commandstring := "WMIC computersystem where caption='" . A_ComputerName . "' rename '" . PcName2 . "'"
			Run %comspec% /c %commandstring%
			Gui, Font, c0xc4e5f6 s10 bold, Courier New
			GuiControl, Font, %_PcCheck%
			GuiControl, Text, %_PcCheck%, -->%PcName2%	
			Gui, Font, cBlack s8 norm, Tahoma
		}
		else {
			font.code
			GuiControl, Font, %_PcCheck%
			GuiControl, Text, %_PcCheck%, no input
		}
	}
	else{
		gui,font, cRed s10 bold, Courier New
		GuiControl, Font, %_NameLen%
		GuiControl, Text, %_NameLen%, %namechars%	
		Gui, Font, cBlack s8 norm, Tahoma
	}
	return
} ;//end//LaunchSysName


LaunchBommaker:
{
	Gui, Submit, NoHide
	if (CityChoice)	{ 
		if (CityChoice != BomCity1)	{ 
			if (CityChoice = BomCity2){ 
				BomCity2 := BomCity1
				BomCity1 := CityChoice
			}
			else if (CityChoice = BomCity3){
				BomCity3 := BomCity1 
				BomCity1 := CityChoice
			}
			else if (CityChoice = BomCity4){
				BomCity4 := BomCity1 
				BomCity1 := CityChoice
			}
			else { ;completely unique BomCity entered, 4th BomCity disappears
				BomCity4 := BomCity3 
				BomCity3 := BomCity2 
				BomCity2 := BomCity1 
				BomCity1 := CityChoice 
			}		
		}	
	}
	ini_replaceValue(bmini, "UserVars", "BomCity1", BomCity1)
	ini_replaceValue(bmini, "UserVars", "BomCity2", BomCity2)
	ini_replaceValue(bmini, "UserVars", "BomCity3", BomCity3)
	ini_replaceValue(bmini, "UserVars", "BomCity4", BomCity4)
	UpdateConfigFile(bminiPath, bmini)
	;; LAUNCH BOMMAKER SCRIPT
	; MSG___(BomLogin BomPassw CityChoice, 3)
	BomDL.init(BomLogin, BomPassw, CityChoice)
	return
} ;//end//LaunchBommaker

GoExitApp:
GuiClose: 
GuiEscape:
{
	gui,destroy
	ExitApp
}



;---------------FUNCTIONS-----------------

MakeRadiosFromArray(aArray, aWHX, aY, aInc, aVarPut, aGroupName, aDef = "blank"){	
	local incr
	incr = %aY%
	for idx, elm in aArray 	{    ; MsgBox % "Element number " . A_Index . " is " . Array[A_Index]
	Gui, Add, Radio, % ( aDef = elm ? "Checked" : "" ) " " aWHX " y"incr " " aVarPut idx " " aGroupName " 0x1000",	%elm%
	incr := incr + aInc
	}
	incr = 0
	return
}
UpdateConfigFile(Path, ByRef Content)
{
	FileDelete, %Path%
	FileAppend, %Content%, %Path%
	Return
}
LocateVISystem(ByRef visfo, ByRef visfi){
	IfExist, C:\Users\%A_UserName%\VIPSystem 
	{
		visfo = yes
	}
	IfExist, C:\Users\%A_UserName%\VIPSystem\settings.ini
	{
		visfi = yes
	}
	return
}
TestUniqueDrive(){
		if(LastDriveSerial = DriveSerial){
		return false
		}
		else{
		return true
		}
}

HideFocusBorder(wParam, lParam := "", uMsg := "", hWnd := "") {
   ; WM_UPDATEUISTATE = 0x0128
	Static Affected := [] ; affected controls / GUIs
        , HideFocus := 0x00010001 ; UIS_SET << 16 | UISF_HIDEFOCUS
	     , OnMsg := OnMessage(0x0128, Func("HideFocusBorder"))
	If (uMsg = 0x0128) { ; called by OnMessage()
      If (wParam = HideFocus)
         Affected[hWnd] := True
      Else If Affected[hWnd]
         PostMessage, 0x0128, %HideFocus%, 0, , ahk_id %hWnd%
   }
   Else If DllCall("IsWindow", "Ptr", wParam, "UInt")
	  PostMessage, 0x0128, %HideFocus%, 0, , ahk_id %wParam%
}

f11::listvars
f8::Pause
f3::Reload
#Include includes/incl_CreateConfigFile.ahk
#Include includes/incl_LaunchSettings.ahk
#Include includes/incl_BomDL.ahk
