﻿; This script was created using Pulover's Macro Creator

#NoEnv
SetWorkingDir %A_ScriptDir%
CoordMode, Mouse, Window
SendMode Input
#SingleInstance Force
SetTitleMatchMode 2
DetectHiddenWindows On
#WinActivateForce
SetControlDelay 1
SetWinDelay 0
SetKeyDelay -1
SetMouseDelay -1
SetBatchLines -1


F3::
Macro1:
Run, chrome.exe "https://support.visualimpressions.net/login/pinned_sessions"
WinWaitActive, LOGIN | Bomgar - Google Chrome
Sleep, 1000
Loop
{
	CoordMode, Pixel, Window
	ImageSearch, FoundX, FoundY, 1, 1, 1920, 1200, C:\Users\Admin\AppData\Roaming\MacroCreator\Screenshots\Screen_20150722191325.png
	Sleep, 100
}
Until ErrorLevel = 0
If ErrorLevel = 0
{
	Click, %FoundX%, %FoundY% Left, 1
	Sleep, 150
	SetKeyDelay, 200
	SendRaw, Todd
	Sleep, 200
	Click, Rel 0, 25 Left, 1
	SetKeyDelay, 100
	SendRaw, Imps2015!
	Sleep, 100
	Send, {Enter}
}
Loop
{
	CoordMode, Pixel, Window
	ImageSearch, FoundX, FoundY, 0, 0, 1920, 1200, C:\Users\Admin\AppData\Roaming\MacroCreator\Screenshots\Screen_20150722182119.png
	Sleep, 100
}
Until ErrorLevel = 0
If ErrorLevel = 0
{
	Send, {Tab 7}
	Sleep, 70
	Send, {Down}
	Sleep, 70
	Send, {Tab 6}
	Sleep, 70
	SetKeyDelay, 70
	SendRaw, Charlotte, NC
	Sleep, 70
	Send, {Tab 6}
	Sleep, 70
	Send, {1 4}
	Sleep, 70
	Send, {Tab 3}
	Sleep, 70
	Send, {Space}
	Sleep, 70
	Send, {Tab}
	Sleep, 70
	Send, {Space}
	Sleep, 70
}
Loop
{
	CoordMode, Pixel, Window
	ImageSearch, FoundX, FoundY, 0, 0, 1920, 1200, C:\Users\Admin\AppData\Roaming\MacroCreator\Screenshots\Screen_20150722182452.png
	Sleep, 100
}
Until ErrorLevel = 0
If ErrorLevel = 0
{
	Click, %FoundX%, %FoundY% Left, 1
	Sleep, 10
	Sleep, 1000
	WinGetPos, chrX, chrY, chrW, chrH, Bomgar - Google Chrome
	Sleep, 1000
	Click, 0, %chrH%, 0
	Sleep, 10
	Sleep, 1000
	Click, Rel 25, -25 Left, 1
	Sleep, 10
}
Return

