﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

run, cmd.exe
sleep 1000

clipboard = netdom renamecomputer %A_ComputerName% /newname:yourcomputer"
msgbox, %clipboard%
sleep 1000
;When you have everything in order just uncomment the below line
;Send !{space}ep  
sleep 1000
Send {Enter}
}
return

exitapp

