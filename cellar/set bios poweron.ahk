Hewlett-Packard�s Client Management Interface and 
Dell�s OpenManage Client Instrumentation allow 
their hardware to be managed through various enterprise 
management tools. After installing the CMI or OMCI client, 
the BIOS on these computers can be accessed using Windows Management Instrumentation.

(PowerShell)


## HP Boot Order
----------------------------------


$bios = Get-WmiObject -Namespace root/hp/instrumentedBIOS -Class hp_biosSetting
($bios | Where-Object {$_.Name -eq 'Boot Order'}).Value.Split(',')



## Dell BootOrder
----------------------------------
Get-WmiObject -Namespace root/dellOMCI -Class Dell_BootDeviceSequence |
Select BootDeviceName, BootOrder, Status |
Sort-Object BootOrder |
Format-Table -AutoSize


## HP BIOS Settings
----------------------------------
Get-WmiObject -Namespace root/hp/instrumentedBIOS -Class hp_biosEnumeration |
Format-Table Name,Value -AutoSize


## Modifying HP Setting
----------------------------------
$bios = Get-WmiObject -Namespace root/hp/instrumentedBIOS -Class HP_BIOSSettingInterface
$bios.SetBIOSSetting('After Power Loss', 'On')


///////////////////////////////////////

After spending a complete day and going through different 
forums and HP's documentation (HP CMI technical whitepaper), 
I finally got this working by the following script. 
Password will be your current password and in plain text.

$AfterPowerr= Get-WmiObject -computername "PCNAME" -Namespace root/hp/instrumentedBIOS -Class HP_BIOSSettingInterface 
$AfterPower.SetBIOSSetting('After Power', 'On ' , '<utf-16/> Password')

///////////////////////////////////////


