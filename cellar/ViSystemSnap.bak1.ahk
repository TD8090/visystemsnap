#NoEnv
SendMode Input
CoordMode, Mouse, Screen
SetWorkingDir %A_ScriptDir%
;save bm_settings.ini file to variable vBMStxt
; MsgBox [, Options, Title, Text, Timeout]
matchTemplate := RegExMatch("O)(\[Client\][[\s\S]*?<\/html>)", %vBMStxt%)
#include, ini.ahk
bminiPath := "bm_settings.ini"
IfNotExist, %bminiPath%
		createConfigFile(bminiPath)
ViSystemFolder = no
ViSystemFile   = no
LocateVISystem(ViSystemFolder, ViSystemFile)

FileRead, bmini, %bminiPath%
Location1 := ini_getValue(bmini, "UserVars", "Location1")
Location2 := ini_getValue(bmini, "UserVars", "Location2")
Location3 := ini_getValue(bmini, "UserVars", "Location3")
Location4 := ini_getValue(bmini, "UserVars", "Location4")
BomCity1 := ini_getValue(bmini, "UserVars", "BomCity1")
BomCity2 := ini_getValue(bmini, "UserVars", "BomCity2")
BomCity3 := ini_getValue(bmini, "UserVars", "BomCity3")
BomCity4 := ini_getValue(bmini, "UserVars", "BomCity4")
PcName1 := ini_getValue(bmini, "UserVars", "PcName1")
PcName2 := ini_getValue(bmini, "UserVars", "PcName2")
PcName3 := ini_getValue(bmini, "UserVars", "PcName3")
PcName4 := ini_getValue(bmini, "UserVars", "PcName4")
BomLogin := ini_getValue(bmini, "UserVars", "BomLogin")
BomPassw := ini_getValue(bmini, "UserVars", "BomPassword")

; LastDriveSerial := ini_getValue(bmini, "UserVars", "LastDriveSerial")

ihashes := ini_getAllKeyNames(bmini, "CommonHashes")
imachesA := ini_getValue(bmini, "UserVars", "MachArrayA")
imachesB := ini_getValue(bmini, "UserVars", "MachArrayB")

hashArray := StrSplit(ihashes, ",")
machArrayA := StrSplit(imachesA, ",")
machArrayB := StrSplit(imachesB, ",")
dfltHash := ini_getValue(bmini, "UserVars", "Hash")
dfltMach := ini_getValue(bmini, "UserVars", "Machine")
finalHashRad := dfltHash
finalMachRad := dfltMach
; value := ini_getValue(bmini, "Client", "Location")
; keys := ini_getAllKeyNames(bmini, "Client")                      ; <- Get a list of all key names.
; sections := ini_getAllSectionNames(bmini)                         ; <- Get a list of all section names.
; msgbox,,, % hashes keys sections value,1

; msgbox,,, % Location1 imachesA machArrayA,1


; GUI ================== GUI ======================== GUI ======================= GUI ===================== GUI ===================== GUI

; GUI ================== GUI ======================== GUI ======================= GUI ===================== GUI ===================== GUI

; GUI ================== GUI ======================== GUI ======================= GUI ===================== GUI ===================== GUI
; 0x02000000 = WS_CLIPCHILDREN, prevents parent window from redrawing over child window
; SetBatchLines, -1
; Keys := {C: "CapsLock", S: "ScrollLock", N: "NumLock"}
; StateColor := {0: 0x5481E6, 1: 0x98CB4A} ; 0 = Off, 1 = On
; Controls := {C: {}, S: {}, N: {}}
; For Ctrl, Key In Keys
; Controls[Ctrl].State := GetKeyState(Key, "T")


Gui, +LastFound +AlwaysOnTop +Border +toolwindow +0x02000000

Gui, Margin, 0, 0

WinSet, Transparent, 10, Vi-Setter
WinMagicMove()
WinSet, Transparent, 255, Vi-Setter
Gui, Show, w400 h300, Vi-Setter
; Gui +LastFound

; gui, color, 8b0fc6
; winset, transcolor, 8b0fc6

LocCheckString := A_UserName "\VISystem ( " ViSystemFolder " ) \settings.ini ( " ViSystemFile " )"
Gui, Add, Text, cGreen x0 y0 h13 w240 HwndLocCheck, %LocCheckString%
Gui, Add, Text, cGreen x260 y0 h13 w120 HwndPcCheck, %SYSname%

;<<COMBO>> LOCATION 
; Font_h1()
Gui, Add, Text, w50 h15 x0 y16 Center, Location
; Font_body()
LocChoices := Location1 "|" Location2 "|" Location3 "|" Location4
Gui, Add, ComboBox, w140 h80 x0 y30 vLocChoice Simple Choose1, %LocChoices%


;<<RADIO>> MACHINE/HASH
Gui, Add, Text, w45 h12 x0 y110 Center, Machine
MakeRadiosFromArray(machArrayA, "w20 h15 x2", 122, 14, "", "gGoRadMaches", dfltMach) 
MakeRadiosFromArray(machArrayB, "w20 h15 x22", 122, 14, "", "gGoRadMaches", dfltMach) 
Gui, Add, Text, w90 h15 x50 y110 Center, Hashes
MakeRadiosFromArray(hashArray, "w90 h15 x50", 122, 14, "", "gGoRadHashes", dfltHash) 


;<<RADIO>> TIMEZONE
Gui, Add, Text,   w50  h15 x300 y60 Center vLabeltz +BackgroundTrans, Timezone
ChosenTimezone = Tz1_East
Gui, Add, Radio,  w85  h15 x300 y75 Checked, Tz1_East 	
Gui, Add, Radio,  w85  h15 x300  y90 gGoTimezone +BackgroundTrans, Tz2_Indiana 
Gui, Add, Radio,  w85  h15 x300 y105 gGoTimezone +BackgroundTrans, Tz3_Central 
Gui, Add, Radio,  w85  h15 x300 y120 gGoTimezone +BackgroundTrans, Tz4_Mountain
Gui, Add, Radio,  w85  h15 x300 y135 gGoTimezone +BackgroundTrans, Tz5_Arizona 
Gui, Add, Radio,  w85  h15 x300 y150 gGoTimezone +BackgroundTrans, Tz6_Pacific 
Gui, Add, Button, w75 h20 x300 y165 vBtnTimezone gLaunchTimezone, Set TimeZone

;<<COMBO>> BOMCITYSTATE 
Gui, Add, Text, w90 h15 x170 y185 Center vLabelbcs, Bomgar: City, ST
BomCityChoices := BomCity1 "|" BomCity2 "|" BomCity3 "|" BomCity4
Gui, Add, ComboBox, w120 h80 x170 y200 vCityChoice Simple Choose1 , %BomCityChoices%
Gui, Add, Text, cPurple x260 y285 w70 h15 Center, %BomLogin%
Gui, Add, Text, cPurple x330 y285 w70 h15 Center, %BomPassw%


;<<COMBO>> PCNAME
Gui, Add, Text, w90 h15 x170 y20 Center, PC Name
PcNameString := PcName1 "|" PcName2 "|" PcName3 "|" PcName4
Gui, Add, ComboBox, w120 h80 x170 y35 vNameChoice Simple Choose1, %PcNameString%
Gui, Add, Button, w100 h20 x170 y115 gLaunchSysName, Change PC Name

;SUBMIT>> LAUNCHERS
Gui, Add, Button, x0 y280 w90 h20 gLaunchSettings default, Write settings
Gui, Add, Button, x170 y280 w90 h20 gLaunchBommaker 0x1 0x1000 0x8000, Make bomclient
; Gui, Add, Button, x380 y0 w20 h20 gGoExitApp, X
Gui, Add, Picture, x0 y0 w400 h300 +0x4000000, VIbg.png


; gui, add, text, x5 y240 w60 h16 0x201 gClicked vS hwndHWND, S
; gui, add, text, x5 y256 w60 h4 0x04 ; 0x04 = SS_BLACKRECT
; Controls.S.Hwnd := HWND

; gui, add, text, x70 y240 w60 h16 0x201 gClicked vN hwndHWND, N
; gui, add, text, x70 y256 w60 h4 0x04 ; 0x04 = SS_BLACKRECT
; Controls.N.Hwnd := HWND

; For Each, Ctrl In Controls
   ; CtlColorStatic(Ctrl.Hwnd, StateColor[Ctrl.State], 0xFFFFFF) 
return
; ExitApp
; /GUI ================== /GUI ======================== /GUI ======================= /GUI ===================== /GUI ===================== /GUI

; /GUI ================== /GUI ======================== /GUI ======================= /GUI ===================== /GUI ===================== /GUI

; /GUI ================== /GUI ======================== /GUI ======================= /GUI ===================== /GUI ===================== /GUI




Clicked:
{
If (A_GuiEvent <> "DoubleClick") && (Ctrl := Controls[A_GuiControl])
  CtlColorStatic(Ctrl.Hwnd, StateColor[Ctrl.State := !Ctrl.State], 0xFFFFFF)
return
}

GetStates:
{
For Ctrl, Key In Keys
	Controls[Ctrl].State := GetKeyState(Key, "T")
Return
}
GoRadHashes:
{	
	finalHashRad = %A_GuiControl%
	return
}
GoRadMaches:
{
	finalMachRad = %A_GuiControl%
	return
}
GoTimezone:
{
	ChosenTimezone = %A_GuiControl%
	return
}
GoExitApp:
{
	gui,destroy
	ExitApp
	return
}

LaunchTimezone:
{
	Gui, Submit, NoHide
	setTimeZone(ChosenTimezone)
	return
} ;//end//LaunchTimezone

LaunchSysName:
{
	Gui, Submit, NoHide
	if(NameChoice){
		if (NameChoice != PcName1)	{ 
			if (NameChoice = PcName2){ 
				PcName2 := PcName1
				PcName1 := NameChoice
			}
			else if (NameChoice = PcName3){
				PcName3 := PcName1 
				PcName1 := NameChoice
			}
			else if (NameChoice = PcName4){
				PcName4 := PcName1 
				PcName1 := NameChoice
			}
			else { 
				PcName4 := PcName3 
				PcName3 := PcName2 
				PcName2 := PcName1 
				PcName1 := NameChoice 
			}		
		}	
	}
	ini_replaceValue(bmini, "UserVars", "PcName1", PcName1)
	ini_replaceValue(bmini, "UserVars", "PcName2", PcName2)
	ini_replaceValue(bmini, "UserVars", "PcName3", PcName3)
	ini_replaceValue(bmini, "UserVars", "PcName4", PcName4)
	updateConfigFile(bminiPath, bmini)
	SetSysName(PcName1)
	Gui, Font, c0xFF0000
	GuiControl, Font, %PcCheck%
	GuiControl, Text, %PcCheck%, ...%PcName1%
	
	
	return
} ;//end//LaunchSysName





LaunchSettings:
{
	Gui, Submit, NoHide
; save [UserVars]Hash
; save [UserVars]Machine
	if (LastDriveSerial != DriveSerial){
		Machplusone := finalMachRad + 1
		ini_replaceValue(bmini, "UserVars", "LastDriveSerial", DriveSerial)
	}else{
		Machplusone := finalMachRad
	}
	if (Machplusone > machArrayA.MaxIndex() + machArrayB.MaxIndex()){
		Machplusone = 1
	}
	ini_replaceValue(bmini, "UserVars", "Machine", Machplusone)
	ini_replaceValue(bmini, "UserVars", "Hash", finalHashRad)

	
	if(LocChoice){
		if (LocChoice != Location1)	{ ;new location isn't already #1, we need to reassign Location variables, else leave the same
			if (LocChoice = Location2){ ; but... it matches stored preset #2 -> swap #2 with #1
				Location2 := Location1 ; make #2 #1
				Location1 := LocChoice ; make choice #1
			}
			else if (LocChoice = Location3){
				Location3 := Location1 
				Location1 := LocChoice
			}
			else if (LocChoice = Location4){
				Location4 := Location1 
				Location1 := LocChoice
			}
			else { ;completely unique location entered, 4th preset location disappears
				Location4 := Location3 
				Location3 := Location2 
				Location2 := Location1 
				Location1 := LocChoice 
			}		
		}	
	}
	ini_replaceValue(bmini, "UserVars", "Location1", Location1)
	ini_replaceValue(bmini, "UserVars", "Location2", Location2)
	ini_replaceValue(bmini, "UserVars", "Location3", Location3)
	ini_replaceValue(bmini, "UserVars", "Location4", Location4)
	
	; write the file
	WrapHead := ini_getKey(bmini, "Browser", "WrapHead")
	WrapFoot := ini_getKey(bmini, "Browser", "WrapFoot")	
	Hashish  := ini_getValue(bmini, "CommonHashes", finalHashRad)
	
	LocateVISystem(ViSystemFolder, ViSystemFile) ;check again 
	NewViSettings := "`n[Client]" 
	 . "`nLocation=" . Location1 
	 . "`nPassword=" . Hashish 
	 . "`nMachine=" . Machplusone
	 . "`n[Browser]" 
	 . "`n" . WrapHead 
	 . "`n" . WrapFoot 
	 
	if (ViSystemFolder = "yes"){
	  if(ViSystemFile = "yes"){
			FileDelete, % "C:\Users\" A_UserName "\VISystem\settings.ini"
			FileAppend, %NewViSettings%, % "C:\Users\" A_UserName "\VISystem\settings.ini"
		  MSG___(">>>updated file", 1)
		}
		else{
			FileAppend, %NewViSettings%, % "C:\Users\" A_UserName "\VISystem\settings.ini"
			FileAppend, %NewViSettings%, % A_ScriptDir "\settings.ini"
		  MSG___(">>>created file", 1)
		}
	}
	else{
		FileCreateDir, % "C:\Users\" A_UserName "\VISystem"
		FileAppend, %NewViSettings%, % "C:\Users\" A_UserName "\VISystem\settings.ini"
		FileAppend, %NewViSettings%, %A_ScriptDir%
	  MSG___(">>>created folder & file", 1)
	}
	FileDelete, % A_ScriptDir "\settings.ini"
	FileAppend, %NewViSettings%, % A_ScriptDir "\settings.ini"
	
	LocateVISystem(ViSystemFolder, ViSystemFile) ;check again 
	GuiControl, Text, %LocCheck%, % "C:\Users\" A_UserName "\VISystem ( " ViSystemFolder " ) \settings.ini ( " ViSystemFile " )"
	Gui, Font, c0xFF0000
	GuiControl, Font, %LocCheck%
	updateConfigFile(bminiPath, bmini)
	return
} ;//end//LaunchSettings

LaunchBommaker:
{
	Gui, Submit, NoHide
	if (CityChoice)	{ 
		if (CityChoice != BomCity1)	{ 
			if (CityChoice = BomCity2){ 
				BomCity2 := BomCity1
				BomCity1 := CityChoice
			}
			else if (CityChoice = BomCity3){
				BomCity3 := BomCity1 
				BomCity1 := CityChoice
			}
			else if (CityChoice = BomCity4){
				BomCity4 := BomCity1 
				BomCity1 := CityChoice
			}
			else { ;completely unique BomCity entered, 4th BomCity disappears
				BomCity4 := BomCity3 
				BomCity3 := BomCity2 
				BomCity2 := BomCity1 
				BomCity1 := CityChoice 
			}		
		}	
	}
	ini_replaceValue(bmini, "UserVars", "BomCity1", BomCity1)
	ini_replaceValue(bmini, "UserVars", "BomCity2", BomCity2)
	ini_replaceValue(bmini, "UserVars", "BomCity3", BomCity3)
	ini_replaceValue(bmini, "UserVars", "BomCity4", BomCity4)
	updateConfigFile(bminiPath, bmini)
	;; LAUNCH BOMMAKER SCRIPT
	RunBommaker(BomLogin, BomPassw, BomCity1)
	return
} ;//end//LaunchBommaker


GuiClose: 
GuiEscape:
{
	gui,destroy
	ExitApp
}


;---------------FUNCTIONS-----------------
Font_h1(){
	Gui, Font, s8 norm, Arial Black
}
Font_h2(){
	Gui, Font, s8 norm, Copperplate Gothic Light
}
Font_body(){
	Gui, Font, s8 norm, Tahoma	
}
Font_code(){
	Gui, Font, s8 norm, Courier New
}

MakeRadiosFromArray(aArray, aWHX, aY, aInc, aVarPut, aGroupName, aDef = "blank"){	
	local incr
	incr = %aY%
	for idx, elm in aArray 	{    ; MsgBox % "Element number " . A_Index . " is " . Array[A_Index]
	Gui, Add, Radio, % ( aDef = elm ? "Checked" : "" ) " " aWHX " y"incr " " aVarPut idx " " aGroupName " 0x1000",	%elm%
	incr := incr + aInc
	}
	incr = 0
	return
}

MSG___(argMsg, argWait){
	Gui, +LastFound -AlwaysOnTop
	msgbox,,, %argMsg%, %argWait%
	Gui, +LastFound +AlwaysOnTop

}
SetSysName(arg){
	; msgbox,,, % "current: " A_ComputerName "`ndesired: " arg  ,2
	Run, cmd.exe, , , PID
	Sleep, 1000	
	Send, % "WMIC computersystem where caption='" . A_ComputerName . "' rename " . arg
	Send, {Enter}
	Sleep, 1000
	Process, Close, %PID%
	Sleep, 1000
	return
}

setTimeZone(arg){
	if (arg = "Tz1_East"){ 
		zone = Eastern Standard Time 
	}
	if (arg = "Tz2_Indiana"){ 
		zone = US Eastern Standard Time 
	}
	if (arg = "Tz3_Central"){ 
		zone = Central Standard Time 
	}
	if (arg = "Tz4_Mountain"){ 
		zone = Mountain Standard Time 
	}
	if (arg = "Tz5_Arizona"){ 
		zone = US Mountain Standard Time 
	}
	if (arg = "Tz6_Pacific"){ 
		zone = Pacific Standard Time 
	}
	; msgbox,,, % "arg: " arg "`nzone: " zone  ,2
	Run, cmd.exe, , , PID
	Sleep, 1000	
	Send, tzutil /s "%zone%"
	Send, {Enter}
	Sleep, 1000
	Gui, +LastFound -AlwaysOnTop
	MSG___("time zone was set to " . TZ, 2)
	Gui, +LastFound +AlwaysOnTop
	Process, Close, %PID%
	Sleep, 1000
	return
}





; Loop, read, bm_settings.txt
; {
	; MsgBox, loopread index %A_Index% is %A_LoopField%.
	; Loop, parse, A_LoopReadLine, =
	; {
	; ARR[A_Index] = A_LoopField
		; MsgBox, Field number %A_Index% is %A_LoopField%.
	; }
; }
; MsgBox, %ARR% yooooo


		; vMONCT = Monitor Count:`t%MonCount%`nPrimary Monitor:`t%MonPrimary%`n
		; vMOXY = `n mouseXY before loop %mox% `t %moy%
		; vSTR .= vMONCT . vMOXY
		; vMONSTAT = 
		; (
		; `nMonitor:%A_Index%
		; Name:%CurrMonName%
		; Left:%CurrMonLeft% (%CurrMonWALeft%)
		; Top:%MonitorTop% (%CurrMonWATop%)
		; Right:%CurrMonRight% (%CurrMonWARight%)
		; Bottom:%CurrMonBottom% (%CurrMonWABottom%)
		; )
		; v
		; STR .= vMONSTAT
		; vSTR = %vSTR% mark

	; Bounds := Object()
	; msgbox % "y minus center" . moy - WinHcen . "beyond top" . adjustY
	; varrr := mox > currMonLeft && mox < currMonRight  ? currMon : return
createConfigFile(Path){
	Template =
	(LTrim
		[UserVars]
		Location1=Cheshire Cat
		Location2=Tweedle Dum
		Location3=Mad Hatter
		Location4=Nature Boy Rick Flair
		Machine=9
		Hash=health
		MachArray=1,2,3,4,5,6,7,8,9,10,11,12
		BomgarCityState=Charlotte, NC
		BomgarLogin=Todd
		BomgarPassword=Vimps2015!
		[CommonHashes]
		blank=d41d8cd98f00b204e9800998ecf8427e
		education=d0bb80aabb8619b6e35113f02e72752b  
		food=62506be34d574da4a0d158a67253ea99
		health=555bf8344ca0caf09b42f55e185526d8
		hotstuff=2522c8a5837a7c180888dfc71ef7bdb2
		insurance=b4628fe4f5f38e5b293be2024ce95239
		salsa=0143c1e8e97da861c623ff508a441c54
		spirits=4d3ff6711adbaeb7060683239d1f445a
		[Client]
		Location=Yale
		Password=d0bb80aabb8619b6e35113f02e72752b
		Machine=4
		[Browser]
		WrapHead=<html><head><style>body,html,table,td,div{cursor:none;}body {background-color:#000000;border:none;color:#ffffff;overflow:hidden;}</style></head><body>
		WrapFoot=</body></html>
	)
	FileAppend, %Template%, %Path%
	Return
}
updateConfigFile(Path, ByRef Content)
{
	FileDelete, %Path%
	FileAppend, %Content%, %Path%
	Return
}
WinMagicMove(){
	MouseGetPos, mox, moy
	WinGetPos,winX,winY,WinW,WinH
	WinHcen := WinH/2, WinWcen = WinW/2
	SysGet, MonCount, MonitorCount
	Loop, %MonCount% {
		SysGet, CurrMon, Monitor, %A_Index%
		SysGet, CurrMonWA, MonitorWorkArea, %A_Index%
		if(mox > CurrMonLeft && mox < CurrMonRight && moy > CurrMonTop && moy < CurrMonBottom){
			if(moy - WinHcen < CurrMonWATop){
				adjustY := Abs(moy - WinHcen - CurrMonWATop)
			}
			if(moy + WinHcen > CurrMonWABottom){
				adjustY := ( moy + WinHcen - CurrMonWABottom ) * -1
			}		
			if(mox - WinWcen < CurrMonWALeft){
				adjustX := Abs(mox - WinWcen - CurrMonWALeft)
			}
			if(mox + WinWcen > CurrMonWARight){
				adjustX := ( mox + WinWcen - CurrMonWARight ) * -1
			}
		}
	}
	y := adjustY ? moy - WinHcen + adjustY : moy - WinHcen
	x := adjustX ? mox - WinWcen + adjustX : mox - WinWcen
	WinMove, x, y
}
LocateVISystem(ByRef visfo, ByRef visfi){
	IfExist, C:\Users\%A_UserName%\VISystem 
	{
		visfo = yes
	}
	IfExist, C:\Users\%A_UserName%\VISystem\settings.ini
	{
		visfi = yes
	}
	return
}
TestUniqueDrive(){
		if(LastDriveSerial = DriveSerial){
		return false
		}
		else{
		return true
		}
}

RunBommaker(argLogin, argPass, argCityState){
	Gui, +LastFound -AlwaysOnTop
	Run, C:\Program Files (x86)\Google\Chrome\Application\chrome.exe "https://support.visualimpressions.net/login/pinned_sessions"
	WinWaitActive, LOGIN | Bomgar - Google Chrome
	Sleep, 100
	WinActivate, LOGIN | Bomgar - Google Chrome
	Sleep, 33
	Loop
	{
		CoordMode, Pixel, Screen
		ImageSearch, FoundX, FoundY, 1, 1, 1920, 1200, bm-username.png
		Sleep, 100
	}
	Until ErrorLevel = 0
	If ErrorLevel = 0
	{
		Click, %FoundX%, %FoundY%, 0
		Sleep, 150
		Click, Left, 1
		Sleep, 10
		SetKeyDelay, 200
		SendRaw, %argLogin%
		Sleep, 200
		Click, Rel 0, 25 Left, 1
		SetKeyDelay, 100
		SendRaw, %argPass%
		Sleep, 100
		Send, {Enter}
	}
	Loop
	{
		CoordMode, Pixel, Screen
		ImageSearch, FoundX, FoundY, 0, 0, 1920, 1200, bm-config.png
		Sleep, 100
	}
	Until ErrorLevel = 0
	If ErrorLevel = 0
	{
		Send, {Tab 7}
		Sleep, 40
		Send, {Down}
		Sleep, 40
		Send, {Tab 6}
		Sleep, 40
		SetKeyDelay, 40
		SendRaw, %argCityState%
		Sleep, 40
		Send, {Tab 6}
		Sleep, 40
		Send, {1 4}
		Sleep, 40
		Send, {Tab 3}
		Sleep, 40
		Send, {Space}
		Sleep, 40
		Send, {Tab}
		Sleep, 70
		Send, {Space}
		Sleep, 70
	}
	Loop
	{
		CoordMode, Pixel, Screen
		ImageSearch, FoundX, FoundY, 0, 0, 1920, 1200, bm-download.png
		Sleep, 100
	}
	Until ErrorLevel = 0
	If ErrorLevel = 0
	{
		Click, %FoundX%, %FoundY%, 0
		Sleep, 150
		Click, Left, 1
		Sleep, 500
		Send, {Shift Down}{Tab 6}{Shift Up}
		Sleep, 20
		Send, {Enter}
		Sleep, 50
		WinGetPos, chrX, chrY, chrW, chrH, Bomgar - Google Chrome
		CoordMode, Mouse, Window
		Sleep, 1000
		aY := chrH - 25
		Sleep, 500
		Click, 25, %aY%, 0
		Sleep, 150
		Click, Left, 1
		CoordMode, Mouse, Screen
	}
	WinWaitActive, Open File - Security Warning
	WinActivate, Open File - Security Warning
	WinMove, 0,0

	Gui, +LastFound +AlwaysOnTop
	Return
}
objWMIService := ComObjGet("winmgmts:{impersonationLevel=impersonate}!\\" . A_ComputerName . "\root\cimv2")

; colBIOS := objWMIService.ExecQuery("SELECT Description, BIOSVersion, SMBIOSBIOSVersion FROM Win32_BIOS")._NewEnum
; While colBIOS[objBIOS] { 
	; BIOSver 		:= objBIOS.BIOSVersion[0]
	; BIOSverver 	:= objBIOS.SMBIOSBIOSVersion[0]
	; BIOSdesc 		:= objBIOS.Description
; }
; colCPU := objWMIService.ExecQuery("SELECT SystemName FROM Win32_Processor")._NewEnum
; While colCPU[objCPU] { 
; PCname := objCPU.SystemName
; }
GetSysName(){
	global
	colSYS := objWMIService.ExecQuery("SELECT Name, BootupState FROM Win32_ComputerSystem")._NewEnum
	While colSYS[objSYS] { 
		SysName := objSYS.Name		
		; msgbox,,,% objSYS.Name		
	}
}
GetSysName()
colDRIVE := objWMIService.ExecQuery("SELECT SerialNumber FROM Win32_DiskDrive")._NewEnum
While colDRIVE[objDRIVE] { 
	DriveSerial := objDRIVE.SerialNumber
}
; msgbox,,, % "`n" DriveSerial "`n" PCname "`n" BIOSver "`n" BIOSverver "`n" SYSname "`n" SYSdesc "`n" BIOSdesc  ,1

f11::listvars
#Include CtlColorStatic.ahk
#Include WinMagicMove.ahk
