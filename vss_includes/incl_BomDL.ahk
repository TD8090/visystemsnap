﻿; BomDL.init("Todd", "Vimps2015!", "CityWok")
; ExitApp
Class BomDL
{
	init(initLogin, initPass, initCityState){
		SendMode Input
		CoordMode, Mouse, Screen
		SetKeyDelay, 20
		Gui, +LastFound -AlwaysOnTop
		global tries := 0, ScW := A_ScreenWidth,	ScH :=  A_ScreenHeight
		Run, C:\Program Files (x86)\Google\Chrome\Application\chrome.exe "https://support.visualimpressions.net/login/pinned_sessions"
		WinWait, LOGIN | Bomgar - Google Chrome		; WinWaitActive, LOGIN | Bomgar - Google Chrome
		WinMove, LOGIN | Bomgar - Google Chrome,, 300, 20, 1200, 500
		Sleep, 33
		Send {Control Down}{0}{Control Up}
		Sleep, 33
		this.ImSearch("vss_assets\bm-username.png")
		if(ErrorLevel = 0){
			this.Step1_Login(initLogin, initPass)
			this.ImSearch("vss_assets\bm-config.png")
			if(ErrorLevel = 0){
				this.Step2_Config(initCityState)	
				this.ImSearch("vss_assets\bm-download.png")
				if(ErrorLevel = 0){
					this.Step3_Download()
				}
			}
		}
		BomDL.BomWrapup()
	}
	ImSearch(ifile){
		global
		CoordMode, Pixel, Screen
		; ImageSearch, FoundX, FoundY, 0, 0, %ScW%, %ScH%, *50 %ifile%
		ImageSearch, FoundX, FoundY, 0, 0, 1920, 1080, *30 %ifile%
		if(ErrorLevel = 0){
			; msgbox,16,, % "found!!!"
			tries := 0
			return 0
		}
		if(ErrorLevel = 1){
			; msgbox,64,, % "...not found...", .1
		}
		if(ErrorLevel = 2){
			; msgbox,64,, % "^^^inability^^^"
		}
		sleep 100
		if(tries < 50){
			tries++
			this.ImSearch(ifile)
			return 1
		}
		else {
			tries := 0
			return 1
		}
	}
	Step1_Login(argLogin, argPass){
		global
		; msgbox,32,, % "@Step1_Login", 1
		BlockInput, SendAndMouse
		Click, %FoundX%, %FoundY%, 0
		Sleep, 150
		Click, Left, 1
		Sleep, 10
		SendRaw, %argLogin%
		Sleep, 150
		Click, Rel 0, 25 Left, 1
		SendRaw, %argPass%
		Sleep, 100
		Send, {Enter}
		BlockInput, Default
	}
	Step2_Config(argCityState){
		global
		; msgbox,32,, % "@Step2_Config", 1
		BlockInput, SendAndMouse
		Send, {Tab 7}
		Sleep, 40
		Send, {Down}
		Sleep, 40
		Send, {Tab 6}
		Sleep, 40
		SendRaw, %argCityState%
		Sleep, 40
		Send, {Tab 6}
		Sleep, 40
		Send, {1 4}
		Sleep, 40
		Send, {Tab 3}
		Sleep, 40
		Send, {Space}
		Sleep, 40
		Send, {Tab}
		Sleep, 70
		Send, {Space}
		Sleep, 70	
		BlockInput, Default
	}
	Step3_Download(){
		global
		BlockInput, SendAndMouse
		; msgbox,32,, % "@Step3_Download", 1
		Click, %FoundX%, %FoundY%, 0
		Sleep, 150
		Click, Left, 1
		Sleep, 300
		Send, {Shift Down}{Tab 6}{Shift Up}
		Sleep, 20
		Send, {Enter}
		Sleep, 20
		WinGetPos, chrX, chrY, chrW, chrH, Bomgar - Google Chrome
		CoordMode, Mouse, Window
		Sleep, 1000
		aY := chrH - 25
		Sleep, 500
		Click, 25, %aY%, 0
		Sleep, 150
		Click, Left, 1
		CoordMode, Mouse, Screen
		WinWait, Open File - Security Warning
		WinActivate, Open File - Security Warning
		WinMove, 0,0	
		BlockInput, Default
	}
	BomWrapup(){
		global
		BlockInput, Default
		; msgbox,,, done!, .5
		Gui, +LastFound +AlwaysOnTop
		WinGetPos, gX, gY, gW, gH, ViSystemSnap
		daX := gX + (gW / 2)
		daY := gY + (gH / 2)
		Click, %daX%, %daY%, 0
		Return
	}
}
