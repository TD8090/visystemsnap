﻿LaunchSettings:
{
	Gui, Submit, NoHide
; save [UserVars]Hash
; save [UserVars]Machine
	if (LastDriveSerial != DriveSerial){
		Machplusone := finalMachRad + 1
		ini_replaceValue(bmini, "UserVars", "LastDriveSerial", DriveSerial)
	}else{
		Machplusone := finalMachRad
	}
	if (Machplusone > machArrayA.MaxIndex() + machArrayB.MaxIndex()){
		Machplusone = 1
	}
	ini_replaceValue(bmini, "UserVars", "Machine", Machplusone)
	ini_replaceValue(bmini, "UserVars", "Hash", finalHashRad)
	
	if(LocChoice){
		if (LocChoice != Location1)	{ ;new location isn't already #1, we need to reassign Location variables, else leave the same
			if (LocChoice = Location2){ ; but... it matches stored preset #2 -> swap #2 with #1
				Location2 := Location1 ; make #2 #1
				Location1 := LocChoice ; make choice #1
			}
			else if (LocChoice = Location3){
				Location3 := Location1 
				Location1 := LocChoice
			}
			else if (LocChoice = Location4){
				Location4 := Location1 
				Location1 := LocChoice
			}
			else { ;completely unique location entered, 4th preset location disappears
				Location4 := Location3 
				Location3 := Location2 
				Location2 := Location1 
				Location1 := LocChoice 
			}		
		}	
	}
	ini_replaceValue(bmini, "UserVars", "Location1", Location1)
	ini_replaceValue(bmini, "UserVars", "Location2", Location2)
	ini_replaceValue(bmini, "UserVars", "Location3", Location3)
	ini_replaceValue(bmini, "UserVars", "Location4", Location4)
	
	; write the file
	WrapHead := ini_getKey(bmini, "Browser", "WrapHead")
	WrapFoot := ini_getKey(bmini, "Browser", "WrapFoot")
	
	Hashish  := ini_getValue(bmini, "CommonHashes", finalHashRad)
	GuiControlGet, Md5input
	if(Md5input != ""){
		Hashish := MD5(Md5input)		
	}
	; msgbox,,,% Hashish
	NewViSettings := "[Client]" 
	 . "`nLocation=" . Location1 
	 . "`nPassword=" . Hashish 
	 . "`nMachine=" . finalMachRad
	 . "`n[Browser]" 
	 . "`n" . WrapHead 
	 . "`n" . WrapFoot 
	 
	if (ViSystemFolder = "yes"){
	  if(ViSystemFile = "yes"){
			FileDelete, %visiniPath%
			FileAppend, %NewViSettings%, %visiniPath%
			Gui, Font, cGreen bold, Tahoma
			GuiControl, Font, %_ViSetReport%
		  GuiControl, Text, %_ViSetReport%, % "> file updated"
		}
		else{
			FileAppend, %NewViSettings%, %visiniPath%
			FileAppend, %NewViSettings%, % A_ScriptDir "\settings.ini"
			Gui, Font, cGreen bold, Tahoma
			GuiControl, Font, %_ViSetReport%
		  GuiControl, Text, %_ViSetReport%, % "> file created"
		}
	}
	else{
		FileCreateDir, % "C:\Users\" A_UserName "\VIPSystem"
		FileAppend, %NewViSettings%, %visiniPath%
		FileAppend, %NewViSettings%, %A_ScriptDir%
		Gui, Font, cGreen bold, Tahoma
		GuiControl, Font, %_ViSetReport%
	  GuiControl, Text, %_ViSetReport%, % ">> folder && file created"
	}
	FileDelete, % A_ScriptDir "\LastWrittenSettings.ini"
	FileAppend, %NewViSettings%, % A_ScriptDir "\LastWrittenSettings.ini"
	UpdateViExistReport()
	UpdateConfigFile(bminiPath, bmini)
	return
} ;//end//LaunchSettings
