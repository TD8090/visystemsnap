createConfigFile(Path){
	Template =
	(LTrim
		[UserVars]
		Location1=White Rabbit
		Location2=Mad Hatter
		Location3=Cheshire Cat
		Location4=Queen of Hearts
		Machine=16
		Hash=spirits
		MachArrayA=1,2,3,4,5,6,7,8
		MachArrayB=9,10,11,12,13,14,15,16
		PcName1=ddd-WEB1117
		PcName2=ddd-WEB1116
		PcName3=ddd-WEB1111a
		PcName4=PROG4
		BomCity1=San Jose, CA
		BomCity2=New York City, NY
		BomCity3=Moltar, NC
		BomCity4=Brak, NC
		BomLogin=UndefinedUser
		BomPassword=UndefinedPass
		LastDriveSerial=UndefinedSerial
		[CommonHashes]
		bagels=518edfb832714692bc5453d5ef70aa10
		education=d0bb80aabb8619b6e35113f02e72752b  
		food=62506be34d574da4a0d158a67253ea99
		health=555bf8344ca0caf09b42f55e185526d8
		hotstuff=2522c8a5837a7c180888dfc71ef7bdb2
		insurance=b4628fe4f5f38e5b293be2024ce95239
		salsa=0143c1e8e97da861c623ff508a441c54
		spirits=4d3ff6711adbaeb7060683239d1f445a
		[Client]
		Location=LocationUndefined
		Password=d41d8cd98f00b204e9800998ecf8427e
		Machine=4
		[Browser]
		WrapHead=<html><head><style>body,html,table,td,div{cursor:none;}body {background-color:#000000;border:none;color:#ffffff;overflow:hidden;}</style></head><body>
		WrapFoot=</body></html>
	)
	FileAppend, %Template%, %Path%
	Return
}