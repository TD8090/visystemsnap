﻿WinMagicMove(){
	MouseGetPos, mox, moy
	if mox = 0
		mox := 1
	if moy = 0
		moy := 1		
	WinGetPos,winX,winY,WinW,WinH
	WinHcen := WinH/2, WinWcen = WinW/2
	SysGet, MonCount, MonitorCount
	Loop, %MonCount% {
		SysGet, CurrMon, Monitor, %A_Index%
		SysGet, CurrMonWA, MonitorWorkArea, %A_Index%
		if(mox > CurrMonLeft && mox < CurrMonRight && moy > CurrMonTop && moy < CurrMonBottom){
			if(moy - WinHcen < CurrMonWATop){
				adjustY := Abs(moy - WinHcen - CurrMonWATop)
			}
			if(moy + WinHcen > CurrMonWABottom){
				adjustY := ( moy + WinHcen - CurrMonWABottom ) * -1
			}		
			if(mox - WinWcen < CurrMonWALeft){
				adjustX := Abs(mox - WinWcen - CurrMonWALeft)
			}
			if(mox + WinWcen > CurrMonWARight){
				adjustX := ( mox + WinWcen - CurrMonWARight ) * -1
			}
		}
	}
	y := adjustY ? moy - WinHcen + adjustY : moy - WinHcen
	x := adjustX ? mox - WinWcen + adjustX : mox - WinWcen
	WinMove, x, y
}