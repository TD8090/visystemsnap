﻿; do NOT set color (cWhite, cBlack, etc) in gui font.  it breaks many styles. (radio 0x1000, combo trans)
; changing font colors post gui, show is OK
; http://ahkscript.org/docs/Objects.htm#Custom_Classes_property


; Class test {
	; Class p	{
		; get := Gui, Font, s20 norm, Tahoma 
	; }
	; t1(){ 
		; Gui, Font, s10 norm, Tahoma
	; }
	; sm()
	; {
		; Gui, Font, s30 norm, Tahoma
	; }
	; w1[]	{
		; get {
			; Gui, Font, s70 norm, Tahoma
		; }
	; }
; }

Class font 
{	
	static small := "s7", medium := "s14", large := "s10"
	p[]	{
		get {
			Gui, Font, %medium% norm, Tahoma
		}
	}
	psmall[]{
		get {
			Gui, Font, s7 norm, Tahoma
		}		
	}
	h1[] {
		get {
			Gui, Font, s8 bold, Copperplate Gothic Bold
		}
	}
	h2[] {
		get {
			Gui, Font, s8 bold, Arial Black
		}
	}
	code[] {
		get {
			Gui, Font, s8 bold, Courier New
		}
	}
}
; Class fcolor 
; {
	; black[]	{
		; get {
			; gui, font, 0x000000
		; }
	; }
	; white[] {
		; get {
			; gui, font, 0xFFFFFF
		; }
	; }
	; ltgrey[] {
		; get {
			; gui, font, 0xBBBBBB
		; }
	; }
	; dkgrey[] {
		; get {
			; gui, font, 0x555555
		; }
	; }
	; orange[] {
		; get {
			; gui, font, 0xCC7700
		; }
	; }
; }
; Class gcolor 
; {
	; black[]	{
		; get {
			; gui, color, 0x000000
		; }
	; }
	; white[] {
		; get {
			; gui, color, 0xFFFFFF
		; }
	; }
	; ltblue[] {
		; get {
			; gui, color, 0xF0F0F0
		; }
	; }
	; ltgrey[] {
		; get {
			; gui, color, 0xF0F0F0
		; }
	; }
	; dkgrey[] {
		; get {
			; gui, color, 0x555555
		; }
	; }
	; orange[] {
		; get {
			; gui, color, 0xCC7700
		; }
	; }
; }

