﻿#NoEnv
SetBatchLines -1
#Include Class_ImageButton.ahk
 
Gui, Color, White
Gui, Font, s10
Gui, Margin, 0, 0
Gui, -Caption +HwndHGUI
ImageButton.SetGuiColor("0xFFFFFF")
 
; GUI Border
Gui, Add, Button, w564 h325 hwndHBT Disabled
	Opt4 := [0, 0xffffff,,,,, 0x1CA7EF, 2]
	ImageButton.Create(HBT, "", "", "", Opt4)
 
	; Title Bar
	Gui, Add, Button, xp+2 yp+2 w560 h27 hwndHBT Disabled, ImageButton Sample
		Opt4 := [0, 0x1CA7EF,, 0xffffff]
		ImageButton.Create(HBT, "", "", "", Opt4)
 
		Gui, Font,, Marlett
	
		; Minimize Button
		Gui, Add, Button, x500 yp w30 h25 hwndHBT gGuiMinimize, 0
			Opt1 := [0, 0x1CA7EF,, 0xCCCCCC]
			Opt2 := [0, 0xC0C0C0,, 0xffffff]
			Opt3 := [0, 0x808080,, 0xffffff]
			ImageButton.Create(HBT, Opt1, Opt2, Opt3)
 
		; Close Button
		Gui, Add, Button, x+0 w30 h25 hwndHBT gGuiClose, r
			Opt1 := [0, 0x1CA7EF,, 0xCCCCCC]
			Opt2 := [0, 0xFF7171,, 0xffffff]
			Opt3 := [0, 0xFF0000,, 0xffffff]
			ImageButton.Create(HBT, Opt1, Opt2, Opt3)
 
	Gui, Font
	Gui, Font, s15 bold
	Gui, Add, Button, x80 y+135 w150 h40 HwndHBT vBtnStart gBtnStart, Start
		Opt1 := [3, 0x46cc9b, 0x228731, "White", 4]
		Opt2 := [3, 0x5bf0b9, 0x35bd47, "White", 4]
		Opt3 := [3, 0x228731, 0x46cc9b, "White", 4]
		Opt4 := [0, 0xF0F0EE,, 0xCACBC2, 4,, 0xCACBC2, 1]
		ImageButton.Create(HBT, Opt1, Opt2, Opt3, Opt4)
	Gui, Add, Button, x+100 wp hp HwndHBT Disabled vBtnStop gBtnStop, Stop
		Opt1 := [3, 0xFF4A4A, 0xCC0000, "White", 4]
		Opt2 := [3, 0xFF6A6A, 0xFF2424, "White", 4]
		Opt3 := [3, 0xEA0000, 0x9B0000, "White", 4]
		Opt4 := [0, 0xF0F0EE,, 0xCACBC2, 4,, 0xCACBC2, 1]
		ImageButton.Create(HBT, Opt1, Opt2, Opt3, Opt4)
 
Gui, Show
Return
 
BtnStart:
	GuiControl, Disable, BtnStart
	GuiControl, Enable, BtnStop
Return
 
BtnStop:
	GuiControl, Disable, BtnStop
	GuiControl, Enable, BtnStart
Return
 
GuiMinimize:
	Gui, Minimize
Return
 
GuiClose:
ExitApp
 
WM_LBUTTONDOWN(wParam, lParam, msg, hwnd) {
	global HGUI
	static nothing := OnMessage(0x0201, "WM_LBUTTONDOWN")
	static cursor := DllCall("LoadCursor", "Uint", 0, "Int", 32646, "Uint") ; SizeAll = 32646
 
	If (hwnd = HGUI) {
		PostMessage, 0xA1, 2
		DllCall("SetCursor", "uint", cursor)
	}
}