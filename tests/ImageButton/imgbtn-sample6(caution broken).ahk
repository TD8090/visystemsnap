﻿#NoEnv
#Include <Class_ImageButton> ; you have to include the class, it's expected to be in the script folder
 
Gui, Destroy
Gui, +AlwaysOnTop
Gui, -Caption +LastFound 
Gui, Color, F0F0F0, F0F0F0			;added Mar 3,2014  hunter99 tranparent test. The F0F0F0 is the MSWindows default color 
WinSet, TransColor, F0F0F0			;added Mar 3,2014  hunter99 tranparent test 
Gui, Add, Button, x2 y2 w120 h24 hwndHBTN1, Test1     ; added hwnd option
Gui, Add, Button, x124 y2 w120 h24 hwndHBTN2, Test2   ; added hwnd option
Gui, Add, Button, x246 y2 w120 h24 hwndHBTN3, Test3   ; added hwnd option
 
; Test1 button: unicolored = 0, - background color = "Lime", text color = "Maroon"
Opt1 := [0, "Lime", , "Maroon", 22]			;test to make round corners, ", 22" added Mar 3,2014  hunter99
ImageButton.Create(HBTN1, Opt1)
; Test2 button: unicolored = 0, - background color = "Yellow", text color = "Navy"
Opt1 := [0, "Yellow", , "Navy", 22]			;test to make round corners, ", 22" added Mar 3,2014  hunter99
ImageButton.Create(HBTN2, Opt1)
; Test3 button: unicolored = 0, - background color = "Red", text color = "Lime"
Opt1 := [0, "Red", , "Lime", 22]			;test to make round corners, ", 22" added Mar 3,2014  hunter99
ImageButton.Create(HBTN3, Opt1)
 
 
Gui, Show, x500 y600 w368 h28, Noactivate
Return
 
ButtonTest1: 
MsgBox You clicked the Test1 button.
Return
 
ButtonTest2: 
MsgBox You clicked the Test2 button.
Return
 
ButtonTest3: 
MsgBox You clicked the Test3 button.
Return
 
GuiEscape:     ;added Mar 3,2014  hunter99
ExitApp
return