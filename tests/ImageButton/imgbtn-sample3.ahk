﻿#NoEnv
SetBatchLines, -1
#Include Class_ImageButton.ahk
 
Gui, Margin, 3, 3
Gui, Font, s10 bold, Segoe UI
 
Gui, Add, Button, vBT1 w151 h27 hwndhBT1, BUTTON 1
Opt1 := [0, 0x80f0f0f0, , 0xD3000000, 2, , 0x800099CC, 1]
Opt2 := [0, 0x8033B5E5, , 0x00FFFFFF, 2, , 0x800099CC, 1]
Opt3 := [0, 0x8033B5E5, , 0x00FFFFFF, 2, , 0x800099CC, 3]
Opt4 := [0, 0x80aceffb, , 0x00FFFFFF, 2, , 0x800099CC, 3]
Opt5 := [0, 0x8033B5E5, , 0x00FFFFFF, 2, , 0x800099CC, 3]
if !(ImageButton.Create(hBT1, Opt1, Opt2, Opt3, Opt4, Opt5))
    MsgBox, 0, ImageButton Error btn1, % ImageButton.LastError
 
Gui, Add, Button, vBT2 w151 h27 hwndhBT2, BUTTON 2
Opt1 := [0, 0x80f0f0f0, , 0xD3000000, 2, , 0x800099CC, 1]
Opt2 := [0, 0x8033B5E5, , 0x00FFFFFF, 2, , 0x800099CC, 1]
Opt3 := [0, 0x8033B5E5, , 0x00FFFFFF, 2, , 0x800099CC, 3]
Opt4 := [0, 0x80aceffb, , 0x00FFFFFF, 2, , 0x800099CC, 3]
Opt5 := [0, 0x8033B5E5, , 0x00FFFFFF, 2, , 0x800099CC, 3]
if !(ImageButton.Create(hBT2, Opt1, Opt2, Opt3, Opt4, Opt5))
    MsgBox, 0, ImageButton Error btn3, % ImageButton.LastError
 
Gui, Add, Button, vBT3 w151 h27 hwndhBT3, BUTTON 3
Opt1 := [0, 0x80f0f0f0, , 0xD3000000, 2, , 0x800099CC, 1]
Opt2 := [0, 0x8033B5E5, , 0x00FFFFFF, 2, , 0x800099CC, 1]
Opt3 := [0, 0x8033B5E5, , 0x00FFFFFF, 2, , 0x800099CC, 3]
Opt4 := [0, 0x80aceffb, , 0x00FFFFFF, 2, , 0x800099CC, 3]
Opt5 := [0, 0x8033B5E5, , 0x00FFFFFF, 2, , 0x800099CC, 3]
if !(ImageButton.Create(hBT3, Opt1, Opt2, Opt3, Opt4, Opt5))
    MsgBox, 0, ImageButton Error btn3, % ImageButton.LastError
 
Gui, Add, Button, vBT4 w151 h27 hwndhBT4, BUTTON 4
Opt1 := [0, 0x80f0f0f0, , 0xD3000000, 2, , 0x800099CC, 1]
Opt2 := [0, 0x8033B5E5, , 0x00FFFFFF, 2, , 0x800099CC, 1]
Opt3 := [0, 0x8033B5E5, , 0x00FFFFFF, 2, , 0x800099CC, 3]
Opt4 := [0, 0x80aceffb, , 0x00FFFFFF, 2, , 0x800099CC, 3]
Opt5 := [0, 0x8033B5E5, , 0x00FFFFFF, 2, , 0x800099CC, 3]
if !(ImageButton.Create(hBT4, Opt1, Opt2, Opt3, Opt4, Opt5))
	MsgBox, 0, ImageButton Error btn4, % ImageButton.LastError
 
Gui, Show, , Image Buttons
return
 
GuiClose:
GuiEscape:
ExitApp
 