﻿#NoEnv
#SingleInstance Force
SetBatchLines -1
 
#Include, Class_ImageButton.ahk
 
Gui, -Caption +Border +HwndHGUI
Gui, Color, 0x4E668C
ImageButton.SetGuiColor("0x4E668C")
 
Gui, Add, Text, w540 h30 0x201 cWhite, test
 
Gui, Font, s12, Marlett
Gui, Add, Button, x+0 yp w30 h30 hwndHBT gGuiMinimize, 0
	Opt1 := [0, 0x182E46,          , 0xffffff,,, 0x4E668C, 1]
	Opt2 := [0, 0x4DA0CA,         , 0xffffff,,, 0x4E668C, 1]
	Opt3 := [3, 0x0984BD, 0x4DA0CA, 0xffffff,,, 0x4E668C, 1]
	ImageButton.Create(HBT, Opt1, Opt2, Opt3)
 
Gui, Add, Button, x+2 w30 h30 hwndHBT gGuiClose, r
	Opt1 := [0, 0x182E46,          , 0xffffff,,, 0x4E668C, 1]
	Opt2 := [0, 0xFF7171,         , 0xffffff,,, 0x4E668C, 1]
	Opt3 := [3, 0xFF0000, 0xFF7171, 0xffffff,,, 0x4E668C, 1]
	ImageButton.Create(HBT, Opt1, Opt2, Opt3)
 
Gui, Font
Gui, Font, s12
 
Gui, Add, Button, xm w605 h150 hwndHBT Disabled
	Opt1 := [0, 0x182E46, 0xffffff, 0x4C658E,,, 0x4C658E, 2]
	ImageButton.Create(HBT, Opt1, Opt1, Opt1, Opt1)
 
	Gui, Add, Text, xp+20 yp+20 wp-40 hp-40 BackgroundTrans cWhite, Is there a way to disable fade-out?
 
Gui, Add, Button, xm w605 h200 hwndHBT Disabled
	Opt1 := [0, 0x182E46, 0xffffff, 0x4C658E,,, 0x4C658E, 2]
	ImageButton.Create(HBT, Opt1, Opt1, Opt1, Opt1)
 
	Gui, Add, Button, xp+20 yp+20 wp-40 hwndHBT1 Left Section, A. Yes
	Gui, Add, Button, xs wp hwndHBT2 Left , B. No
	Gui, Add, Button, xs wp hwndHBT3 Left , C. Yes
	Gui, Add, Button, xs wp hwndHBT4 Left , D. No
		Opt1 := [0, 0x182E46, 0xffffff, 0xffffff]
		Opt2 := [4, 0xB72D2D, 0x182E46, 0xffffff]
		Loop, 4
			ImageButton.Create(HBT%A_Index%, Opt1, Opt2, Opt1, Opt1)
 
Gui, Show
Return
 
GuiClose:
ExitApp
 
GuiMinimize:
	Gui, Minimize
Return
 
WM_LBUTTONDOWN(wParam, lParam, msg, hwnd) {
	static WM_LBUTTONDOWN := OnMessage(0x0201, "WM_LBUTTONDOWN")
	global HGUI
 
	If (hwnd = HGUI)
		PostMessage, 0xA1, 2
}