﻿#NoEnv
#Persistent
#SingleInstance, Force
SetWorkingDir, %A_ScriptDir%
#Include Class_ImageButton.ahk
 
IfNotExist, Pic1.png
URLDownloadToFile, https://www.dropbox.com/s/a9nlbr8wojkagdk/Pic1.png?dl=1, Pic1.png
IfNotExist, Pic2.png
URLDownloadToFile, https://www.dropbox.com/s/xeandzj1w1ny203/Pic2.png?dl=1, Pic2.png
 
Gui, example: -SysMenu -DPIScale
Gui, example: Margin, 0, 0
Gui, example: Add, Button, x100 y4 w16 h14 +hwndEXMPLBTN
Gui, example: Add, Picture, x100 y4 +hwndEXMPLBTNPIC, Pic1.png
SendMessage, 0x0173, 0, 0,, ahk_id %EXMPLBTNPIC%
EXMPLBTNPICBITMAP := ErrorLevel
Opt1 := [0, EXMPLBTNPICBITMAP]
Opt3 := {2:"Pic2.png"}
ImageButton.Create(EXMPLBTN, Opt1,, Opt3)
Gui, example: Color, 0xD4D0C8
Gui, example: Show, w120 h20, example
Return
 
GuiClose:
GuiEscape:
ExitApp