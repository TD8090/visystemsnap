﻿#NoEnv
SetBatchLines, -1
#Include Class_ImageButton.ahk
 
Gui, Margin, 50, 20
Gui, Font, s14 bold, Segoe UI
 
Gui, Add, Button, vBT1 w200 h65 hwndhBT1, ECOMMERCE
Opt1 := [0, 0x80f0f0f0, , 0xD308d860, 2, , 0x8008d860, 3]
Opt2 := [, 0x8008d860, , 0x00FFFFFF]
Opt5 := [ , ,  ,0x00FFFFFF]
if !(ImageButton.Create(hBT1, Opt1, Opt2, , , Opt5))
    MsgBox, 0, ImageButton Error btn1, % ImageButton.LastError
 
 
Gui, Add, Button, vBT2 w200 h65 hwndhBT2, MOBILE APPS
Opt1 := [0, 0x80f0f0f0, , 0xD30bb1d0, 2, , 0x800bb1d0, 3]
Opt2 := [0, 0x800bb1d0, , 0x00FFFFFF]
Opt3 := [0, 0x800bb1d0, , 0x00FFFFFF]
Opt4 := [0, 0x80aceffb, , 0x00FFFFFF, 2, , 0x807ce5f8, 3]
Opt5 := [0, 0x800bb1d0, , 0x00FFFFFF]
if !(ImageButton.Create(hBT2, Opt1, Opt2, Opt3, Opt4, Opt5))
    MsgBox, 0, ImageButton Error btn3, % ImageButton.LastError
 
Gui, Add, Button, vBT3 w200 h65 hwndhBT3 disabled, MOBILE APPS
Opt1 := [0, 0x80f0f0f0, , 0xD30bb1d0, 2, , 0x800bb1d0, 3]
Opt2 := [0, 0x800bb1d0, , 0x00FFFFFF]
Opt3 := [0, 0x807ce5f8, , 0x00FFFFFF]
Opt4 := [0, 0x80aceffb, , 0x00FFFFFF, 2, , 0x804bdbf5, 3]
Opt5 := [0, 0x800bb1d0, , 0x00FFFFFF]
if !(ImageButton.Create(hBT3, Opt1, Opt2, Opt3, Opt4, Opt5))
    MsgBox, 0, ImageButton Error btn3, % ImageButton.LastError
 
Gui, Add, Button, vBT4 w200 h65 hwndhBT4, MARKETING
Opt1 := [0, 0x80f0f0f0, , 0xD3d84896, 2, , 0x80d84896, 3]
Opt2 := [ , 0xD3d84896, , 0x00FFFFFF]
Opt5 := [ , , ,0x00FFFFFF]
if !(ImageButton.Create(hBT4, Opt1, Opt2, , , Opt5))
	MsgBox, 0, ImageButton Error btn4, % ImageButton.LastError
 
Gui, Show, , Image Buttons
return
 
GuiClose:
GuiEscape:
ExitApp
 