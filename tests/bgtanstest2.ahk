﻿;==========================================================
; Setup
;----------------------------------------------------------
; basic script header
#NoEnv 
#SingleInstance, Force 
#Persistent 
#MaxThreadsPerHotkey 2 
;#maxThreads 50 ; default is 10
SetTitleMatchMode, 2 
SetBatchLines, -1 
SetKeyDelay, 50,50 
DetectHiddenWindows, On 
CoordMode, Pixel, Screen
CoordMode, Mouse, Screen
CoordMode, Tooltip, Screen
;SendMode, InputThenPlay

centerx := (A_ScreenWidth // 2)
centery := (A_ScreenHeight // 2)

gosub BarGui
return

;====================================================================
; End of Autoexecute
;====================================================================

;====================================================================
; Gui Construction Proof of Concept
;====================================================================
BarGui:
	;............................................
	; bar setup values
	guititle := Text Button Gui
	backcolor := 0xf0c060
	textcolor := 0x004300
	
	GuiPosX := (A_ScreenWidth // 2)
	GuiposY := (A_ScreenHeight // 2)
	
	Gui,color, %backcolor%
	gui -sysmenu ;<-- this makes the title bar buttons go away
	Gui, Add, Text, x10 y10 c%textcolor% gtxtbutton, BUTTON

	Gui Show, x%GuiPosX% y%GuiPosY% w60 h60, %guititle%
	Return


txtbutton:
	Msgbox, you clicked on the text
	return

esc::ExitApp