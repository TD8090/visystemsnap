﻿TextBackgroundColor := 0xFFBBBB  ; A custom color in BGR format.
TextBackgroundBrush := DllCall("CreateSolidBrush", UInt, TextBackgroundColor)

Gui, Color, , D9D0C3
Gui +LastFound
GuiHwnd := WinExist()
Gui, Add, ComboBox, x20 y20 w130 R5 HwndMyTextHwnd vTEST, 1|2|3
Gui, Show, xCenter yCenter, Window

WindowProcNew := RegisterCallback("WindowProc", ""  ; Specifies "" to avoid fast-mode for subclassing.
    , 4, MyTextHwnd)  ; Must specify exact ParamCount when EventInfo parameter is present.
WindowProcOld := DllCall("SetWindowLong", UInt, GuiHwnd, Int, -4  ; -4 is GWL_WNDPROC
    , Int, WindowProcNew, UInt)  ; Return value must be set to UInt vs. Int.

Gui Show
WinSet, Redraw,, ahk_id %GuiHwnd%
return


WindowProc(hwnd, uMsg, wParam, lParam)
{
    Critical
    global TextBackgroundColor, TextBackgroundBrush, WindowProcOld
    if (uMsg = 0x134 && lParam = A_EventInfo)  ; WM_CTLCOLORCOMBOBOX := 0x134
    {
        DllCall("SetBkColor", UInt, wParam, UInt, TextBackgroundColor)
        return TextBackgroundBrush  ; Return the HBRUSH to notify the OS that we altered the HDC.
    }
    ; Otherwise (since above didn't return), pass all unhandled events to the original WindowProc.
    return DllCall("CallWindowProcA", UInt, WindowProcOld, UInt, hwnd, UInt, uMsg, UInt, wParam, UInt, lParam)
}

GuiClose:
ExitApp