﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#NoEnv
SetBatchLines, -1
Items := ["0000", "0001", "0010", "0011", "0100", "0101", "0110", "0111", "1000", "1001", "1010", "1011", "1100"
        , "1101", "1110", "1111", "1111|0000"]
PrevText := ""
Gui, Add, ComboBox, vSel w400 r10 hwndHCBB gAutoComplete
For Each, Item In Items
   CB_AddItem(HCBB, Item)
CB_SetCueBanner(HCBB, "... will be auto-completed!")
Gui, Show, , ComboBox AutoComplete Sample
Return
; ----------------------------------------------------------------------------------------------------------------------
AutoComplete:
   GuiControlGet, EditText, , %A_GuiControl%, Text
   If (EditText <> "") && ((EditText . "") <> (PrevText . "")) && (Item := CB_FindItem(HCBB, EditText, 0)) {
      ItemText := CB_GetItemText(HCBB, Item)
      If (EditText . "") = (ItemText . "")
         CB_SelectItem(HCBB, Item)
      Else {
         GuiControl, Text, %A_GuiControl%, %ItemText%
         CB_SetEditSel(HCBB, StrLen(EditText) + 1, 0)
      }
   }
   PrevText := EditText
Return
; ----------------------------------------------------------------------------------------------------------------------
GuiClose:
ExitApp
; ----------------------------------------------------------------------------------------------------------------------
#Include CB.ahk



#NoEnv
SetBatchLines, -1
CBAttr := ["A", "D", "E", "H", "R", "S", "V"]
Gui, Margin, 10, 10
Gui, Add, Text, , Select a folder:
Gui, Add, Edit, xm y+2 w350 +ReadOnly vDir
Gui, Add, Button, x+10 yp hp w40 +Default gSelectFolder, ...
Gui, Add, Text, xm, Show:
Gui, Add, CheckBox, xm y+2 w100 vCBV gAttrChanged, Drives
Gui, Add, CheckBox, x+0 yp w100 vCBD gAttrChanged, Directories
Gui, Add, CheckBox, x+0 yp w100 vCBA gAttrChanged, Archived
Gui, Add, CheckBox, x+0 yp w100 vCBH gAttrChanged, Hidden
Gui, Add, CheckBox, xm y+2 w100 vCBR gAttrChanged, Read-only
Gui, Add, CheckBox, x+0 yp w100 vCBS gAttrChanged, Sytem
Gui, Add, CheckBox, x+0 yp w100 vCBE gAttrChanged, Exclusive
Gui, Add, ComboBox, xm w400 r10 +Simple gComboSelect hwndHCBB vShow
CB_SetCueBanner(HCBB, "Select a drive [-c-] or folder [folder]!")
Gui, Show, , ComboBox ShowDir Sample
Return
; ----------------------------------------------------------------------------------------------------------------------
SelectFolder:
FileSelectFolder, Folder, ::{20d04fe0-3aea-1069-a2d8-08002b30309d}, 4, Select a folder:
If (ErrorLevel)
   Return
If (SubStr(Folder, StrLen(Folder)) = "\")
   Folder := SubStr(Folder, 1, -1)
GuiControl, , Dir, %Folder%
GoSub, ShowDir
Return
; ----------------------------------------------------------------------------------------------------------------------
AttrChanged:
GoSub, ShowDir
Return
; ----------------------------------------------------------------------------------------------------------------------
ComboSelect:
Gui, Submit, NoHide
If !CB_GetSelected(HCBB)
   Return
If (SubStr(Show, 1, 2) = "[-") ; drive
   Dir := SubStr(Show, 3, -2) . ":"
Else If (SubStr(Show, 1, 1) = "[") { ; folder
   Show := SubStr(Show, 2, -1)
   If (Show <> "..")
      Dir .= "\" . Show
   Else
      Dir := SubStr(Dir, 1, InStr(Dir, "\", 0, 0) - 1)
}
Else
   Return
GuiControl, , Dir, %Dir%
GoSub, ShowDir
Return
; ----------------------------------------------------------------------------------------------------------------------
ShowDir:
Gui, Submit, NoHide
If (Dir = "")
   Return
Attr := ""
For Each, A In CBAttr
   If (CB%A%)
      Attr .= A
CB_ShowDir(HCBB, Dir . "\*.*", Attr)
Return
; ----------------------------------------------------------------------------------------------------------------------
GuiClose:
ExitApp
; ----------------------------------------------------------------------------------------------------------------------
#Include CB.ahk