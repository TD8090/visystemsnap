﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
; ======================================================================================================================
; Function:       Functions for AHK ComboBox controls and, with reservations, DDL controls.
; Namespace:      CB
; Tested with:    AHK 1.1.22.00 (A32/U32/U64)
; Tested on:      Win 8.1 (x64)
; Changelog:
;     0.9.00.00/2015-05-01/just me     - initial release
; Common Parameters:
;     HCB      -  The handle (HWND) of the control.
;     Item     -  The 1-based index of a list item.
; Remarks:
;     All indices passed to and returned by the functions are (expected to be) 1-based.
; Reference:
;     msdn.microsoft.com/en-us/library/ff485901(v=vs.85).aspx
; ======================================================================================================================
CB_AddItem(HCB, ItemText) {
   ; CB_ADDSTRING = 0x0143
   SendMessage, 0x0143, 0, % &ItemText, , ahk_id %HCB%
   Return ((ErrorLevel = "FAIL") || (ErrorLevel & 0x80000000)) ? 0 : ++ErrorLevel
}
; ======================================================================================================================
CB_DeleteAllItems(HCB) {
   ; CB_RESETCONTENT = 0x014B
   SendMessage, 0x014B, 0, 0, , ahk_id %HCB%
   Return (ErrorLevel = "FAIL") ? False : True
}
; ======================================================================================================================
CB_DeleteItem(HCB, Item) {
   ; CB_DELETESTRING = 0x0144
   SendMessage, 0x0144, % --Item, 0, , ahk_id %HCB%
   Return ((ErrorLevel = "FAIL") || (ErrorLevel & 0x80000000)) ? False : True
}
; ======================================================================================================================
CB_FindItem(HCB, String, Start) {
   ; CB_FINDSTRING = 0x014C
   SendMessage, 0x014C, % --Start, % &String, , ahk_id %HCB%
   Return ((ErrorLevel = "FAIL") || (ErrorLevel & 0x80000000)) ? 0 : ++ErrorLevel
}
; ======================================================================================================================
CB_FindItemExact(HCB, String, Start) {
   ; CB_FINDSTRINGEXACT = 0x0158
   SendMessage, 0x0158, % --Start, % &String, , ahk_id %HCB%
   Return ((ErrorLevel = "FAIL") || (ErrorLevel & 0x80000000)) ? 0 : ++ErrorLevel
}
; ======================================================================================================================
CB_GetItemCount(HCB) {
   ; CB_GETCOUNT = 0x0146
   SendMessage, 0x0146, 0, 0, , ahk_id %HCB%
   Return ((ErrorLevel = "FAIL") || (ErrorLevel & 0x80000000)) ? 0 : ErrorLevel
}
; ======================================================================================================================
CB_GetItemHeight(HCB) {
   ; CB_GETITEMHEIGHT = 0x0154
   SendMessage, 0x0154, 0, 0, , ahk_id %HCB%
   Return ((ErrorLevel = "FAIL") || (ErrorLevel & 0x80000000)) ? 0 : ErrorLevel
}
; ======================================================================================================================
CB_GetEditControl(HCB) {
   ; HEDIT: Offset = 40 + A_PtrSize, Type = "UPtr"
   Return CB_GetInfo(HCB, CBBI, 40 + A_PtrSize, "UPtr")
}
; ======================================================================================================================
CB_GetEditSel(HCB, ByRef Start, ByRef End) {
   ; CB_GETEDITSEL = 0x0140
   Start := End := 0
   SendMessage, 0x0140, 0, 0, , ahk_id %HCB%
   If (ErrorLevel = "FAIL")
      Return False
   DWORD := ErrorLevel
   VarSetCapacity(WORD, 2, 0)
   NumPut(DWORD & 0xFFFF, WORD, 0, "Short")
   Start := NumGet(WORD, 0, "Short") + 1
   NumPut((DWORD >> 16) & 0xFFFF, WORD, 0, "Short")
   End := NumGet(WORD, 0, "Short") + 1
   Return True
}
; ======================================================================================================================
CB_GetInfo(HCB, ByRef CBBI, OffSet := -1, Type := "") {
   ; CB_GETCOMBOBOXINFO = 0x0164
   Static SizeOfCBBI := 40 + (A_PtrSize * 3)
   VarSetCapacity(CBBI, SizeOfCBBI, 0)
   NumPut(SizeOfCBBI, CBBI, 0, "UInt")
   SendMessage, 0x0164, 0, &CBBI, , ahk_id %HCB%
   Return ((ErrorLevel = 0) || (ErrorLevel = "FAIL")) ? False
          : ((OffSet > 0) && (Type <> "")) ? NumGet(CBBI, Offset, Type)
          : True
   ; Return DllCall("User32.dll\GetComboBoxInfo", "Ptr", HCB, "Ptr", &CBBI, "UInt") ? True : False
}
; ======================================================================================================================
CB_GetItemData(HCB, Item) {
   ; CB_GETITEMDATA = 0x0150
   SendMessage, 0x0150, % --Item, 0, , ahk_id %HCB%
   Return ((ErrorLevel = "FAIL") || (ErrorLevel & 0x80000000)) ? 0 : ErrorLevel
}
; ======================================================================================================================
CB_GetItemText(HCB, Item) {
   ; CB_GETLBTEXT = 0x0148
   If (TextLength := CB_GetItemTextLength(HCB, Item)) {
      VarSetCapacity(ItemText, TextLength * 2, 0)
      SendMessage, 0x0148, % --Item, % &ItemText, , ahk_id %HCB%
      VarSetCapacity(ItemText, -1)
      Return ItemText
   }
}
; ======================================================================================================================
CB_GetItemTextLength(HCB, Item) {
   ; CB_GETLBTEXTLEN = 0x0149
   SendMessage, 0x0149, % --Item, 0, , ahk_id %HCB%
   Return ((ErrorLevel = "FAIL") || (ErrorLevel & 0x80000000)) ? 0 : ErrorLevel
}
; ======================================================================================================================
CB_GetListControl(HCB) {
   ; HLIST: Offset = 40 + (A_PtrSize * 2), Type = "UPtr"
   Return CB_GetInfo(HCB, CBBI, 40 + A_PtrSize + A_PtrSize, "UPtr")
}
; ======================================================================================================================
CB_GetMinVisible(HCB) {
   ; CB_GETMINVISIBLE = 0x1702
   SendMessage, 0x1702, 0, 0, , ahk_id %HCB%
   Return (ErrorLevel = "FAIL") ? 0 : ErrorLevel
}
; ======================================================================================================================
CB_GetSelected(HCB) {
   ; CB_GETCURSEL = 0x0147
   SendMessage, 0x0147, 0, 0, , ahk_id %HCB%
   Return ((ErrorLevel = "FAIL") || (ErrorLevel & 0x80000000)) ? 0 : ++ErrorLevel
}
; ======================================================================================================================
CB_GetTopItem(HCB) {
   ; CB_GETTOPINDEX = 0x015B
   SendMessage, 0x015B, 0, 0, , ahk_id %HCB%
   Return ((ErrorLevel = "FAIL") || (ErrorLevel & 0x80000000)) ? 0 : ++ErrorLevel
}
; ======================================================================================================================
CB_InsertItem(HCB, Item, ItemText) {
   ; CB_INSERTSTRING = 0x014A
   SendMessage, 0x014A, % --Item, % &ItemText, , ahk_id %HCB%
   Return ((ErrorLevel = "FAIL") || (ErrorLevel & 0x80000000)) ? 0 : ++ErrorLevel
}
; ======================================================================================================================
CB_LimitEditText(HCB, CharsCount) {
   ; CB_LIMITTEXT = 0x0141
   SendMessage, 0x0141, %CharsCount%, 0, , ahk_id %HCB%
   Return (ErrorLevel = "FAIL") ? False : True
}
; ======================================================================================================================
CB_SelectItem(HCB, Item) {
   ; CB_SETCURSEL = 0x014E
   SendMessage, 0x014E, % --Item, 0, , ahk_id %HCB%
   Return ((ErrorLevel = "FAIL") || (ErrorLevel & 0x80000000)) ? 0 : ErrorLevel
}
; ======================================================================================================================
CB_ShowDir(HCB, FilePattern, Attributes) {
   ; CB_DIR = 0x0145 -> msdn.microsoft.com/en-us/library/bb775832(v=vs.85).aspx
   ; DDL_ARCHIVE   = 0x0020 : A - Includes archived files.
   ; DDL_DIRECTORY = 0x0010 : D - Includes subdirectories.
   ; DDL_DRIVES    = 0x4000 : V - All mapped drives are added to the list.
   ; DDL_EXCLUSIVE = 0x8000 : E - Includes only files with the specified attributes.
   ; DDL_HIDDEN    = 0x0002 : H - Includes hidden files.
   ; DDL_READONLY  = 0x0001 : R - Includes read-only files.
   ; DDL_READWRITE = 0x0000 :   - Includes read/write files with no additional attributes. This is the default.
   ; DDL_SYSTEM    = 0x0004 : S - Includes system files.
   Static Attr := {A: 0x0020, D: 0x0010, E: 0x8000, H: 0x0002, R: 0x0001, S: 0x0004, V: 0x4000}
   If Attributes Is Integer
      Attributes &= 0xA037
   Else {
      A := 0
      Loop, Parse, Attributes
         If Attr.HasKey(A_LoopField)
            A |= Attr[A_LoopField]
      Attributes := A
   }
   CB_DeleteAllItems(HCB)
   SendMessage, 0x0145, %Attributes%, % &FilePattern, , ahk_id %HCB%
   Return ((ErrorLevel = "FAIL") || (ErrorLevel & 0x80000000)) ? 0 : ++ErrorLevel
}
; ======================================================================================================================
CB_SetCueBanner(HCB, Cue) {
   ; EM_SETCUEBANNER = 0x1501
   If (HEDIT := CB_GetEditControl(HCB))
      Return DllCall("SendMessageW", "Ptr", HEDIT, "UInt", 0x1501, "Ptr", True, "WStr", Cue, "UInt")
   Return False
}
; ======================================================================================================================
CB_SetEditSel(HCB, Start, End) {
   ; CB_SETEDITSEL = 0x0142
   Selection := ((Start - 1) & 0xFFFF) | (((End - 1) & 0xFFFF) << 16)
   SendMessage, 0x0142, 0, %Selection%, , ahk_id %HCB%
   Return ((ErrorLevel = "FAIL") || (ErrorLevel & 0x80000000)) ? False : True
}
; ======================================================================================================================
CB_SetItemData(HCB, Item, Data) {
   ; CB_SETITEMDATA = 0x0151
   SendMessage, 0x0151, % --Item, %Data%, , ahk_id %HCB%
   Return ((ErrorLevel = "FAIL") || (ErrorLevel & 0x80000000)) ? False : True
}
; ======================================================================================================================
CB_SetItemHeight(HCB, Height) {
   ; CB_SETITEMHEIGHT = 0x0153
   SendMessage, 0x0153, 0, %Height%, , ahk_id %HCB%
   Return ((ErrorLevel = "FAIL") || (ErrorLevel & 0x80000000)) ? False : True
}
; ======================================================================================================================
CB_SetMinVisible(HCB, Min) {
   ; CB_SETMINVISIBLE = 0x1701
   SendMessage, 0x1701, %Min%, 0, , ahk_id %HCB%
   Return (ErrorLevel = "FAIL") ? False : ErrorLevel
}
; ======================================================================================================================
CB_SetTopItem(HCB, Item) {
   ; CB_SETTOPINDEX = 0x015C
   SendMessage, 0x015C, % --Item, 0, , ahk_id %HCB%
   Return ((ErrorLevel = "FAIL") || (ErrorLevel & 0x80000000)) ? False : True
}