﻿GUIFonts = Abadi MT Condensed Light,Arial,Arial Alternative Regular,Arial Alternative Symbol,Arial Black,Arial Bold,Arial Bold Italic,Arial Italic,Book Antiqua,Calisto MT,Century Gothic,Century Gothic Bold,Century Gothic Bold Italic,Century Gothic Italic,Comic Sans MS,Comic Sans MS Bold,Copperplate Gothic Bold,Copperplate Gothic Light,Courier,Courier New,Courier New Bold,Courier New Bold Italic,Courier New Italic,Estrangelo Edessa,Franklin Gothic Medium,Franklin Gothic Medium Italic,Gautami,Georgia,Georgia Bold,Georgia Bold Italic,Georgia Italic,Georgia Italic Impact,Impact,Latha,Lucida Console,Lucida Handwriting Italic,Lucida Sans Italic,Lucida Sans Unicode,Marlett,Matisse ITC,Modern,Modern MS Sans Serif,MS Sans Serif,MS Serif,Mv Boli,News Gothic MT,News Gothic MT Bold,News Gothic MT Italic,OCR A Extended,Palatino Linotype,Palatino Linotype Bold,Palatino Linotype Bold Italic,Palatino Linotype Italic,Roman,Script,Small Fonts,Smallfonts,Symbol,Tahoma,Tahoma Bold,Tempus Sans ITC,Times New Roman,Times New Roman Bold,Times New Roman Bold Italic,Times New Roman Italic,Trebuchet,Trebuchet Bold,Trebuchet Bold Italic,Trebuchet Italic,Trebuchet MS,Trebuchet MS Bold,Trebuchet MS Bold Italic,Trebuchet MS Italic,Tunga,Verdana,Verdana Bold ,Verdana Bold Italic,Verdana Italic,Webdings,Westminster,Wingdings,WST_Czech,WST_Engl,WST_Fren,WST_Germ,WST_Ital,WST_Span, WST_Swed
 
Gui , Fonts:Font , s14 
StringSplit , GUIArray_ , GUIFonts , `, 
width := 250  ;  column width
rows:=20  ;  Number of fonts per column
Loop , %GUIArray_0%
{
	opt=
	If ((a_index/rows)=1 || (a_index/rows)=2 || (a_index/rows)=3 || (a_index/rows)=4 || (a_index/rows)=5) 
		opt=Section ym
	fontname := GUIArray_%a_index%
	Gui , Fonts:Font , s12 , %fontname%
	Gui , Fonts:Add , Text , w%width% %opt% , %fontname%
	Gui , Fonts:Font , Normal
}
Gui , Fonts:Show ,  , AHK Fonts Displayed
Exit
 
FontsGuiClose:
FontsGuiEscape:
ExitApp