﻿; #Include incl_md5.ahk

; Gui, Add, Text, w40 h15 x10  y5   , Word
; Gui, Add, Edit, w320 h15 x50  y5  vMd5Input
; Gui, Add, Text, w40 h15 x10  y20   , Type
; Gui, Add, Button, w145 h25 x2 y44   gSubmitMd5, 

; Gui, Add, Edit, w320 h15 x50  y20 disabled hwnd_Md5ResultWord
; Gui, Add, Edit, w320 h15 x50  y20 disabled hwnd_Md5ResultHash

Gui, +Toolwindow +AlwaysOnTop
  
 

Gui, Add, Edit, 	w90  h17 x+5 yp vMd5input,
Gui, Add, Button, w50  h17 x+2 yp gHashify, Hashify
Gui, Add, Button, 	w234  h17 x160 yp vMd5output gCopyMD5
Gui, Add, Text, 	w36  h17 x400 yp+2 vCopyMD5Report
GuiControl, Hide, Md5output
GuiControl, Hide, CopyMD5Report


Gui Show, x80 y500 w700, md5 vert

; style an edit control
Gui, Font, CWhite S7
Gui, Color,, Red
GuiControl, Font, Md5input
GuiControl, MoveDraw, Md5input
Gui, Font, cBlack S8, Courier New
GuiControl, Font, Md5output



return
CopyMD5:
{
	GuiControl, Show, CopyMD5Report
	Gui, Submit, NoHide
  Clipboard := Md5output
	GuiControl,, CopyMD5Report, Copied! 
	Sleep 1000
	GuiControl, Hide, CopyMD5Report

	return
}

Hashify:
{
	Gui, Submit, NoHide
	GuiControl,, Md5output,   % MD5(Md5input)
	GuiControl, Show, Md5output
	GuiControlGet, Md5output, Pos
	str := "education"
	; Gui Show, AutoSize, md5 vert
	Gui, Submit, NoHide
}

; usage 	GuiControl,, Md5output,   % MD5(Md5input)

MD5(string, encoding = "UTF-8")
{
    return CalcStringHash(string, 0x8003, encoding)
}
; CalcStringHash ====================================================================
CalcStringHash(string, algid, encoding = "UTF-8", byref hash = 0, byref hashlength = 0)
{
    chrlength := (encoding = "CP1200" || encoding = "UTF-16") ? 2 : 1
    length := (StrPut(string, encoding) - 1) * chrlength
    VarSetCapacity(data, length, 0)
    StrPut(string, &data, floor(length / chrlength), encoding)
    return CalcAddrHash(&data, length, algid, hash, hashlength)
}
CalcAddrHash(addr, length, algid, byref hash = 0, byref hashlength = 0)
{
    static h := [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, "a", "b", "c", "d", "e", "f"]
    static b := h.minIndex()
    hProv := hHash := o := ""
    if (DllCall("advapi32\CryptAcquireContext", "Ptr*", hProv, "Ptr", 0, "Ptr", 0, "UInt", 24, "UInt", 0xf0000000))
    {
        if (DllCall("advapi32\CryptCreateHash", "Ptr", hProv, "UInt", algid, "UInt", 0, "UInt", 0, "Ptr*", hHash))
        {
            if (DllCall("advapi32\CryptHashData", "Ptr", hHash, "Ptr", addr, "UInt", length, "UInt", 0))
            {
                if (DllCall("advapi32\CryptGetHashParam", "Ptr", hHash, "UInt", 2, "Ptr", 0, "UInt*", hashlength, "UInt", 0))
                {
                    VarSetCapacity(hash, hashlength, 0)
                    if (DllCall("advapi32\CryptGetHashParam", "Ptr", hHash, "UInt", 2, "Ptr", &hash, "UInt*", hashlength, "UInt", 0))
                    {
                        loop % hashlength
                        {
                            v := NumGet(hash, A_Index - 1, "UChar")
                            o .= h[(v >> 4) + b] h[(v & 0xf) + b]
                        }
                    }
                }
            }
            DllCall("advapi32\CryptDestroyHash", "Ptr", hHash)
        }
        DllCall("advapi32\CryptReleaseContext", "Ptr", hProv, "UInt", 0)
    }
    return o
}




f3::Reload

