Option Explicit
On Error Resume Next
Private WshShell
Private oAdminLink
Private oPF86Link
Private oAdminStartPin
Private oPF86StartPin

Private destLinksFolder
Private destPinsFolder
Private linkHomeFolder
Private linkPF86Folder

set WshShell = WScript.CreateObject("WScript.Shell")
destLinksFolder = WshShell.ExpandEnvironmentStrings("%USERPROFILE%") & "\Links"
destPinsFolder = WshShell.ExpandEnvironmentStrings("%AppData%") & "\Microsoft\Internet Explorer\Quick Launch\User Pinned\StartMenu"
linkHomeFolder = WshShell.ExpandEnvironmentStrings("%USERPROFILE%")
linkPF86Folder = "C:\Program Files (x86)"

set oAdminLink = WshShell.CreateShortcut(destLinksFolder & "\Admin.lnk")
oAdminLink.TargetPath = linkHomeFolder
oAdminLink.WorkingDirectory = destLinksFolder
oAdminLink.Save

set oPF86Link = WshShell.CreateShortcut(destLinksFolder & "\Program Files (x86).lnk")
oPF86Link.TargetPath = linkPF86Folder
oPF86Link.WorkingDirectory = destLinksFolder
oPF86Link.Save

set oAdminStartPin = WshShell.CreateShortcut(destPinsFolder & "\Admin.lnk")
oAdminStartPin.TargetPath = linkHomeFolder
oAdminStartPin.WorkingDirectory = destPinsFolder
oAdminStartPin.Save

set oPF86StartPin = WshShell.CreateShortcut(destPinsFolder & "\Program Files (x86).lnk")
oPF86StartPin.TargetPath = linkPF86Folder
oPF86StartPin.WorkingDirectory = destPinsFolder
oPF86StartPin.Save

