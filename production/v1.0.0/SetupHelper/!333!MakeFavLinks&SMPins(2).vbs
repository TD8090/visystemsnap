Option Explicit
On Error Resume Next
Private WshShell
Private oAdminLink
Private oPF86Link
Private oAdminStartPin
Private oPF86StartPin

Private p_Favs
Private p_Pins
Private p_User
Private p_PF86

set WshShell = WScript.CreateObject("WScript.Shell")
p_Favs = WshShell.ExpandEnvironmentStrings("%USERPROFILE%") & "\Links"
p_Pins = WshShell.ExpandEnvironmentStrings("%AppData%") & "\Microsoft\Internet Explorer\Quick Launch\User Pinned\StartMenu"
p_User = WshShell.ExpandEnvironmentStrings("%USERPROFILE%")
p_PF86 = "C:\Program Files (x86)"

set oAdminLink = WshShell.CreateShortcut(p_Favs & "\Admin.lnk")
oAdminLink.TargetPath = p_User
oAdminLink.WorkingDirectory = p_Favs
oAdminLink.Save

set oPF86Link = WshShell.CreateShortcut(p_Favs & "\Program Files (x86).lnk")
oPF86Link.TargetPath = p_PF86
oPF86Link.WorkingDirectory = p_Favs
oPF86Link.Save

set oAdminStartPin = WshShell.CreateShortcut(p_Pins & "\Admin.lnk")
oAdminStartPin.TargetPath = p_User
oAdminStartPin.WorkingDirectory = p_Pins
oAdminStartPin.Save

set oPF86StartPin = WshShell.CreateShortcut(p_Pins & "\Program Files (x86).lnk")
oPF86StartPin.TargetPath = p_PF86
oPF86StartPin.WorkingDirectory = p_Pins
oPF86StartPin.Save

