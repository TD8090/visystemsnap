﻿SetTimeZone(arg){
	if (arg = "East"){ 
		zone = Eastern Standard Time 
	}
	if (arg = "Indiana"){ 
		zone = US Eastern Standard Time 
	}
	if (arg = "Central"){ 
		zone = Central Standard Time 
	}
	if (arg = "Mountain"){ 
		zone = Mountain Standard Time 
	}
	if (arg = "Arizona"){ 
		zone = US Mountain Standard Time 
	}
	if (arg = "Pacific"){ 
		zone = Pacific Standard Time 
	}
	; msgbox,,, % "arg: " arg "`nzone: " zone  ,2	
	commandstring = tzutil /s `"%zone%`"
	Run %comspec% /c %commandstring%
	GuiControl, Text, %_tzreport%, % "TZ: " . zone
	return
}