@ECHO off
bcdedit /set {default} BootStatusPolicy IgnoreAllFailures 
bcdedit /set {default} RecoveryEnabled No
bcdedit /set {default} PAE ForceEnable
bcdedit /set {default} QuietBoot On
bcdedit /set IncreaseUserVA 3072
pause