::______REMOTE ACCESS
::~ r3: Remote Access Auto Connection Manager - May be required for directcable/DSL providers, depending on logon implementation
sc config RasAuto start= disabled
::~ r4: Remote Access Connection Manager - Manages dial-up and VPN connections to the Internet or other remote networks
sc config RasMan start= disabled
::~ r3: Remote Desktop Configuration(RDCS) - responsible for all RemDesktop Services and RemDesktop related config 
::~       and session maint activities that require SYSTEM context like per-session temp folders, RD themes, and RD certs.
sc config SessionEnv start= disabled
::~ r3: Remote Desktop Services - Allows users to connect interactively to a remote computer
sc config TermService start= disabled
::~ r3: Remote Desktop Services UserMode Port Redirector - Allows the redirection of Printers/Drives/Ports for RDP connections
sc config UmRdpService start= disabled
::~ r0: Remote Registry - Always disable it for Security purposes
sc config RemoteRegistry start= disabled
PAUSE