#NoEnv
SetBatchLines -1 
ListLines Off
SendMode Input
SetWinDelay, 10
CoordMode, Mouse, Screen
#Include vss_includes/incl_ini.ahk
#Include vss_includes/incl_WinMagicMove.ahk
#include vss_includes/incl_Fonts.ahk
#Include vss_includes/incl_ImageButton.ahk
; SetWorkingDir %A_ScriptDir%
; MsgBox [, Options, Title, Text, Timeout]
bminiPath := "vss_assets\config.vss.ini"
visiniPath := "C:\Users\" A_UserName "\VIPSystem\settings.ini"
IfNotExist, %bminiPath%
		CreateConfigFile(bminiPath)

FileRead, bmini, %bminiPath%
Location1 := ini_getValue(bmini, "UserVars", "Location1")
Location2 := ini_getValue(bmini, "UserVars", "Location2")
Location3 := ini_getValue(bmini, "UserVars", "Location3")
Location4 := ini_getValue(bmini, "UserVars", "Location4")
BomCity1 := ini_getValue(bmini, "UserVars", "BomCity1")
BomCity2 := ini_getValue(bmini, "UserVars", "BomCity2")
BomCity3 := ini_getValue(bmini, "UserVars", "BomCity3")
BomCity4 := ini_getValue(bmini, "UserVars", "BomCity4")
PcName1 := ini_getValue(bmini, "UserVars", "PcName1")
PcName2 := ini_getValue(bmini, "UserVars", "PcName2")
PcName3 := ini_getValue(bmini, "UserVars", "PcName3")
PcName4 := ini_getValue(bmini, "UserVars", "PcName4")
BomLogin := ini_getValue(bmini, "UserVars", "BomLogin")
BomPassw := ini_getValue(bmini, "UserVars", "BomPassword")
LocChoices := Location1 "|" Location2 "|" Location3 "|" Location4
BomCityChoices := BomCity1 "|" BomCity2 "|" BomCity3 "|" BomCity4
PcNameString := PcName1 "|" PcName2 "|" PcName3 "|" PcName4

; LastDriveSerial := ini_getValue(bmini, "UserVars", "LastDriveSerial")

ihashes := ini_getAllKeyNames(bmini, "CommonHashes")
imachesA := ini_getValue(bmini, "UserVars", "MachArrayA")
imachesB := ini_getValue(bmini, "UserVars", "MachArrayB")
hashArray := StrSplit(ihashes, ",")
machArrayA := StrSplit(imachesA, ",")
machArrayB := StrSplit(imachesB, ",")
dfltHash := ini_getValue(bmini, "UserVars", "Hash")
dfltMach := ini_getValue(bmini, "UserVars", "Machine")
finalHashRad := dfltHash
finalMachRad := dfltMach

objWMIService := ComObjGet("winmgmts:{impersonationLevel=impersonate}!\\" . A_ComputerName . "\root\cimv2")
GetSysName(){
	global
	colSYS := objWMIService.ExecQuery("SELECT Name, BootupState FROM Win32_ComputerSystem")._NewEnum
	While colSYS[objSYS] { 
		SysName := objSYS.Name		
		; msgbox,,,% objSYS.Name		
	}
}
; GetSysName()
; colDRIVE := objWMIService.ExecQuery("SELECT SerialNumber FROM Win32_DiskDrive")._NewEnum
; While colDRIVE[objDRIVE] { 
	; DriveSerial := objDRIVE.SerialNumber
; }
; msgbox,,, % "`n" DriveSerial "`n" PCname "`n" BIOSver "`n" BIOSverver "`n" SYSname "`n" SYSdesc "`n" BIOSdesc  ,1




; GUI ================== GUI ======================== GUI ======================= GUI ===================== GUI ===================== GUI
; GUI ================== GUI ======================== GUI ======================= GUI ===================== GUI ===================== GUI

; +0x02000000 = WS_CLIPCHILDREN, prevents parent window from redrawing over child window
; +0x4000000 = WS_CLIPSIBLINGS  Clips child windows relative to each other
; LastFound activates our new window. required for MagicMove and maybe more
Gui, +LastFound +AlwaysOnTop -Caption +hwnd_ViSystemSnap
Gui, Add, Picture, x414 y145, vss_assets/VSSTitle.png

; test.p
; test.t1

font.p
gui, color, 0x555555
AddCtrls_ViSettings()
UpdateViExistReport()
AddCtrls_PcName()
PCNameLengthReporter()
AddCtrls_Timezone()
InitTZ()
AddCtrls_GetStatus()
AddCtrls_Bomgar()
AddCtrls_Openers()
AddCtrls_Md5Hashifier()
;<<PICTURE>> BACKGROUND 
; Gui, Add, Picture, x0 y0 w420 h300 +0x4000000, vss_assets/VIbg.png

font.p
font.h1
GuiControl, Font, %_H1loc%
GuiControl, Font, %_H1mach%
GuiControl, Font, %_H1hash%
GuiControl, Font, %_H1pcname%
GuiControl, Font, %_H1tzone%
GuiControl, Font, %_H1status%
font.code
GuiControl, Font, %_NameChoice%
GuiControl, Font, %_BOMLOG%
GuiControl, Font, %_BOMPAS%
font.psmall
GuiControl, Font, %_OPENADMI%
GuiControl, Font, %_OPENPF86%
GuiControl, Font, %_OPENINST%
font.h2
GuiControl, Font, %_H1bom%
font.p

Gui, Font, CWhite S8
Gui, Color,, Black
GuiControl, Font, Md5input
GuiControl, Font, CopyMD5Report
GuiControl, MoveDraw, Md5input
GuiControl, Font, NameChoice
GuiControl, MoveDraw, NameChoice
Gui, Font, cBlack S8, Courier New
GuiControl, Font, Md5output

; gcolor.ltgrey
; gcolor.dkgrey

Gui, Show, Hide
WinMagicMove()
Gui, Show, w430 h340, ViSystemSnap
Gui, Add, Picture, w430 h340 x0 y0 +BackgroundTrans gUiMove
HideFocusBorder(_ViSystemSnap)

; ExitApp
; /GUI ================== /GUI ======================== /GUI ======================= /GUI ===================== /GUI ===================== /GUI
; /GUI ================== /GUI ======================== /GUI ======================= /GUI ===================== /GUI ===================== /GUI

; -- END AUTOEXECUTE
return
; -- END AUTOEXECUTE

AddCtrls_ViSettings(){ ;SETTINGS
	global
	Gui, Add, Button, w149 h253 x0 y16 Disabled hwnd_STATUSBackGround, 
	GSbg := [0, 0xF0F0F0, 0xF0F0F0, "Black", 0, 0xF0F0F0]
	ImageButton.Create(_STATUSBackGround, GSbg, GSbg)
	Gui, Add, Text, 		w340 h15 x5  y0   hwnd_ViExistReport, %LocCheckString%
	Gui, Add, Text, 		w65  h12 x5  y16  hwnd_H1loc Center +BackgroundTrans, Location
	Gui, Add, ComboBox, w140 h80 x5  y30  vLocChoice Simple Choose1, %LocChoices%
	Gui, Add, Text, 		w75  h12 x5  y108 hwnd_H1mach +BackgroundTrans, Machine
	MakeRadiosFromArray(machArrayA, "w20 h15 x12", 122, 14, "", "gGoRadMaches", dfltMach) 
	MakeRadiosFromArray(machArrayB, "w20 h15 x32", 122, 14, "", "gGoRadMaches", dfltMach) 
	Gui, Add, Text, 		w72  h12 x73 y108 hwnd_H1hash +BackgroundTrans, Hashes
	MakeRadiosFromArray(hashArray, "w90 h15 x54", 122, 14, "", "gGoRadHashes", dfltHash) 
	Gui, Add, Button, 	w90  h15 x5  y236 hwnd_LauSet gLaunchSettings, Write settings
	Gui, Add, Button, 	w43  h15 x100  y236 hwnd_OpenVini gOpenVini, Open
	BlueLt1 := [3, 0xaec9d7, 0x608fa7, "Black", 5, 0xF0F0F0]
	BlueLt2 := [3, 0xDD0000, 0x550000, "White", 5, 0xF0F0F0]
	BlueLt3 := [3, 0x608fa7, 0xaec9d7, "White", 5, 0xF0F0F0]
	ImageButton.Create(_LauSet, BlueLt1, BlueLt2, BlueLt3)
	ImageButton.Create(_OpenVini, BlueLt1, BlueLt2, BlueLt3)
	Gui, Add, Text, c0x608fa7 w140 h13 x5 yp+16 hwnd_ViSetReport +BackgroundTrans, --
	return
}
AddCtrls_Md5Hashifier(){	; Md5Hashifier
	global
	Gui, Add, Edit, 	w100  h17 x0      y269 vMd5input,
	Gui, Add, Button, w18   h17 x+0     yp    gClrHashify, X
	Gui, Add, Button, w31   h17 x+0     yp    gHashify, MD5
	Gui, Add, Button, w234  h19 x0   y+0    vMd5output gCopyMD5
	Gui, Add, Text,   w36   h19 xp+240  yp+4 vCopyMD5Report  +BackgroundTrans
	GuiControl, Hide, Md5output
	GuiControl, Hide, CopyMD5Report
	return
}
AddCtrls_PcName(){ ;PCNAME
	global
	Gui, Add, Text, 		w65  h12 x156 y20 hwnd_H1pcname, PC Name
	Gui, Add, Text, 		w15  h12 x+5 yp hwnd_NAMELEN, --
	Gui, Add, Text, 		w160 h13 x+0 yp hwnd_PcCheck, %SYSname%
	Gui, Add, ComboBox, w112 h85 x156 yp+14 vNameChoice hwnd_NameChoice gEditPC Simple Choose1 Uppercase, %PcNameString%
	Gui, Add, Button, 	w66 h15 x156 yp+84 gLaunchSysName hwnd_LauPcNm, Rename PC
	Gui, Add, Button, 	w42 h15 x226 yp gCheckSysName hwnd_LauChkNm, Inspect
	BlueDk1 := [3, 0xaec9d7, 0x608fa7, "Black", 5, 0x555555]
	BlueDk2 := [3, 0xDD0000, 0x550000, "White", 5, 0x555555]
	BlueDk3 := [3, 0x608fa7, 0xaec9d7, "White", 5, 0x555555]
	ImageButton.Create(_LauPcNm, BlueDk1, BlueDk2, BlueDk3)
	ImageButton.Create(_LauChkNm, BlueDk1, BlueDk2, BlueDk3)
	return
}
AddCtrls_Timezone(){ ;TIMEZONE
	global
	Gui, Add, Text,   w65  h15 x154  y138  hwnd_H1tzone, Timezone
	Gui, Add, Radio,  w70  hp   xp+5 yp+15 gGoTimezone, East 	
	Gui, Add, Radio,  w70  hp   xp   yp+15 gGoTimezone, Indiana 
	Gui, Add, Radio,  w70  hp   xp   yp+15 gGoTimezone, Central 
	Gui, Add, Radio,  w70  hp   xp   yp+15 gGoTimezone, Mountain
	Gui, Add, Radio,  w70  hp   xp   yp+15 gGoTimezone, Arizona 
	Gui, Add, Radio,  w70  hp   xp   yp+15 gGoTimezone, Pacific 
	Gui, Add, Button, w75  hp xp-5   yp+18  +0x1000 gLaunchTimezone hwnd_LauTz, Set TimeZone
	ImageButton.Create(_LauTz, BlueDk1, BlueDk2, BlueDk3)
	Gui, Add, Text, c0xc4e5f6 w130 h13 xp+3 yp+16 hwnd_tzreport +BackgroundTrans, --
	return
}
AddCtrls_GetStatus(){ ;GET STATUS
	global
	Gui, Add, Button, w150 h102 x275 y34 Disabled hwnd_STATUSBackGround
	GSbg := [4, 0xFF000000, 0x555555, "Black", 9, 0x555555, 0x4a1d00, 2]
	ImageButton.Create(_STATUSBackGround, GSbg, GSbg)
	Gui, Add, Button, w120 h15 xp+15 yp+3 hwnd_LauStat gLaunchGetStatus, Get Status
	Grey1 := [0, 0x4a1d00,, 0xcc7700, 0, 0x555555, 0xcc7700, 1]
	Grey2 := [0, 0xfd6100,, "White", 0, 0x555555, 0xcc7700, 1]
	Grey3 := [0, 0x555555,, "White", 0, 0x555555, 0xcc7700, 1]
	ImageButton.Create(_LauStat,Grey1,Grey2,Grey3)
	Gui, Add, Text, c0xfd6100  w12  h15  xp-12 yp+18  +BackgroundTrans hwnd_VIL, L:
	Gui, Add, Text, c0xfd6100  w12  h15  xp    yp+16  +BackgroundTrans hwnd_VIM, M:
	Gui, Add, Text, c0xfd6100  w12  h15  xp    yp+16  +BackgroundTrans hwnd_VIP, P:
	Gui, Add, Text, c0xfd6100  w12  h15  xp    yp+16  +BackgroundTrans hwnd_VIH, H:
	Gui, Add, Text, c0xfd6100  w52  h15  xp    yp+16  +BackgroundTrans hwnd_VIB, #Boms:
	Gui, Add, Text, c0xCB8300  w134 h15  xp+12 yp-64  +BackgroundTrans hwnd_VILOC , --
	Gui, Add, Text, c0xCB8300  w134 h15  xp    yp+16  +BackgroundTrans hwnd_VIMACH, --
	Gui, Add, Text, c0xCB8300  w134 h15  xp    yp+16  +BackgroundTrans hwnd_VIPASS, --
	Gui, Add, Text, c0xCB8300  w134 h15  xp    yp+16  +BackgroundTrans hwnd_VIHASH, --
	Gui, Add, Text, c0xCB8300  w60  h15  xp+26 yp+16  +BackgroundTrans hwnd_VIBOMN, --
	return
}
AddCtrls_Bomgar(){	;BOMGAR 
	global
	Gui, Add, Button, w120 h142 x288 y140 Disabled hwnd_BOMBackGround, 
	BMbg := [0, 0xCC7700, 0x654D1E, "Black", 6, 0x555555]
	ImageButton.Create(_BOMBackGround, BMbg, BMbg)
	Gui, Add, Text,     wp h12 xp yp  hwnd_H1bom Center +BackgroundTrans, % "B o m - M a k e r"
	Gui, Add, ComboBox, wp h80 xp yp+15 vCityChoice Simple Choose1, %BomCityChoices%
	Gui, Add, Button,   wp h15 xp yp+80 gLaunchBommaker hwnd_BtnMakeBom, Make Bomgar
	BomOpt1 := [3, 0xCC7700, 0xffa82e, "Black", 5, 0xCC7700]
	BomOpt2 := [3, 0xDD0000, 0x550000, "White", 5, 0xCC7700]
	BomOpt3 := [3, 0xffa82e, 0xCC7700, "White", 5, 0xCC7700]
	ImageButton.Create(_BtnMakeBom, BomOpt1, BomOpt2, BomOpt3)
	Gui, Add, Text,     wp h14 x284 yp+17 hwnd_BOMLOG Center +BackgroundTrans, %BomLogin%
	Gui, Add, Text,     wp h14 x284 yp+15 hwnd_BOMPAS Center +BackgroundTrans, %BomPassw%
	return
}
AddCtrls_Openers(){	;OPENERS
	global 
	Gui, Add, Button,   w40 h12 x2   y312 hwnd_OPENADMI gOpenAdmin 	  , Admin
	Gui, Add, Button,   w40 h12 x2   y326 hwnd_OPENPF86 gOpenPF86 		, PF86
	Gui, Add, Button,   w50 h12 x44  y312 hwnd_OPENAPPW gOpenAppwiz , AppWiz
	Gui, Add, Button,   w50 h12 x44  y326 hwnd_OPENINST gOpenInstaller, Installer
	Gui, Add, Button,   w45 h12 x96 y312 hwnd_OPENNONO gOpenNoNotif, NoNotif
	Gui, Add, Button,   w45 h12 x96 y326 hwnd_OPENFAVP gOpenFavPins, FavPins
	Gui, Add, Button,   w45 h12 x143 y312 hwnd_OPENBCDE gOpenBCDEdits, BCDEdit
	Gui, Add, Button,   w45 h12 x143 y326 hwnd_OPENPOWR gOpenPowerPlan, PowPlan
	Gui, Add, Button,   w45 h12 x190 y312 hwnd_OPENSYSP gOpenSystemProp, SysProp
	Gui, Add, Button,   w45 h12 x190 y326 hwnd_OPENNETD gOpenNetAdapters, NetDevs
	Gui, Add, Button,   w30 h12 x238 y298 hwnd_OPENDISP gOpenDisplay, Disp
	Gui, Add, Button,   w30 h12 x238 y312 hwnd_OPEN1920 gOpen1920, % "1920"
	Gui, Add, Button,   w30 h12 x238 y326 hwnd_OPEN1280 gOpen1280, % "1280"
	Gui, Add, Button,   w50 h12 x270 y298 hwnd_OPENBKUP gOpenContentBackup, % "contbkup"
	Gui, Add, Button,   w20 h12 x284 y312 hwnd_OPENDLVI gOpenDeleteVIP, % "Dlt"
	Gui, Add, Button,   w50 h12 x270 y326 hwnd_OPENBKDN gOpenContentImport, % "contimpt"
	Gui, Add, Button,   w40 h12 x322 y312 hwnd_OPENSVON gOpenServicesOn, % "SvcOn"
	Gui, Add, Button,   w40 h12 x322 y326 hwnd_OPENSVOF gOpenServicesOff, % "SvcOff"
	Gui, Add, Button,   w64 h15 x365 y292 hwnd_CONFPOWR gConfirmPower
	Gui, Add, Button,   w64 h15 x365 y308 hwnd_OPENSHUT gAskShutdown, % "Shutdown"
	Gui, Add, Button,   w64 h15 x365 y324 hwnd_OPENREBT gAskReboot, % "Reboot"
	Gui, Add, Button,   w53 h20 x350 y0   hwnd_BtnKiller gGoKiller, F9:KillAll
	Gui, Add, Button,   w20 h20 x410 y0   hwnd_BtnCloseX gGuiClose , X
	EXOpt1 := [7, 0xE5B300,   0xFF000000, 0xE5B300, , 0x555555, "Gray", 1]
	EXOpt2 := [0, 0xE5B300,   0x555555, "Black", , 0x555555, "Gray", 1]
	EXOpt3 := [7, 0xFF000000, 0x654D1E, "White", , 0x555555, "Gray", 1]
	OpOpt1 := [3, 0xCC7700, 0xffa82e, "Black", 5, 0x555555]
	OpOpt2 := [3, 0xDD0000, 0x550000, "White", 5, 0x555555]
	OpOpt3 := [3, 0xffa82e, 0xCC7700, "White", 5, 0x555555]
	ImageButton.Create(_OPENADMI, OpOpt1, OpOpt2, OpOpt3)
	ImageButton.Create(_OPENPF86, OpOpt1, OpOpt2, OpOpt3)
	ImageButton.Create(_OPENINST, OpOpt1, OpOpt2, OpOpt3)
	ImageButton.Create(_OPENAPPW, OpOpt1, OpOpt2, OpOpt3)
	ImageButton.Create(_OPENNONO, OpOpt1, OpOpt2, OpOpt3)
	ImageButton.Create(_OPENFAVP, OpOpt1, OpOpt2, OpOpt3)
	ImageButton.Create(_OPENBCDE, OpOpt1, OpOpt2, OpOpt3)
	ImageButton.Create(_OPENPOWR, OpOpt1, OpOpt2, OpOpt3)
	ImageButton.Create(_OPENSYSP, OpOpt1, OpOpt2, OpOpt3)
	ImageButton.Create(_OPENNETD, OpOpt1, OpOpt2, OpOpt3)
	ImageButton.Create(_OPENDISP, OpOpt1, OpOpt2, OpOpt3)
	ImageButton.Create(_OPEN1920, OpOpt1, OpOpt2, OpOpt3)
	ImageButton.Create(_OPEN1280, OpOpt1, OpOpt2, OpOpt3)
	ImageButton.Create(_OPENSVON, OpOpt1, OpOpt2, OpOpt3)
	ImageButton.Create(_OPENSVOF, OpOpt1, OpOpt2, OpOpt3)
	ImageButton.Create(_OPENBKUP, OpOpt1, OpOpt2, OpOpt3)
	ImageButton.Create(_OPENDLVI, OpOpt1, OpOpt2, OpOpt3)
	ImageButton.Create(_OPENBKDN, OpOpt1, OpOpt2, OpOpt3)
	ImageButton.Create(_OPENSHUT, EXOpt1, EXOpt2, EXOpt3)
	ImageButton.Create(_OPENREBT, EXOpt1, EXOpt2, EXOpt3)
	ImageButton.Create(_CONFPOWR, EXOpt1, EXOpt2, EXOpt3)
	ImageButton.Create(_BtnKiller, EXOpt1, EXOpt2, EXOpt3)
	ImageButton.Create(_BtnCloseX, EXOpt1, EXOpt2, EXOpt3)
	GuiControl, Hide, %_CONFPOWR%
	return
}
EditPC:
{
	PCNameLengthReporter()
	return
}
PCNameLengthReporter(){
	global
	Gui, Submit, NoHide
	namechars := StrLen(NameChoice)
	if(namechars < 16){
		gui,font, cGreen s10 bold, Courier New
		GuiControl, Font, %_NameLen%
		GuiControl, Text, %_NameLen%, %namechars%	
		Gui, Font, cBlack s8 norm, Tahoma
	}
	else{	
		gui,font, cRed s10 bold, Courier New
		GuiControl, Font, %_NameLen%
		GuiControl, Text, %_NameLen%, %namechars%	
		Gui, Font, cBlack s8 norm, Tahoma
	}
	return
}
ClrHashify:
{
	GuiControl,, Md5input, % ""
	GuiControl, Hide, Md5output
	GuiControl, Hide, CopyMD5Report
	return
}
OpenContentBackup:
{	
	IfNotExist, C:\Users\%A_UserName%\zVIPContent
		FileCreateDir, C:\Users\%A_UserName%\zVIPContent
	FileCopy, C:\Users\%A_UserName%\VIPSystem\Content\*.*, C:\Users\%A_UserName%\zVIPContent\*.*, 1
	return
}
OpenDeleteVIP:
{	
	Gui, Submit, NoHide
	msgbox,4,, % "This Deletes C:\Users\" A_UserName "\VIPSystem`nAre you sure?"
	IfMsgBox, Yes
		FileRemoveDir, C:\Users\%A_UserName%\VIPSystem, 1
	Gui, Font, cRed bold
	GuiControl, Text, %_ViSetReport%, % "> file removed"
	GuiControl, Font, %_ViSetReport%
	UpdateViExistReport()
	return	
}
UpdateViExistReport(){
	global
	IfExist, C:\Users\%A_UserName%\VIPSystem 
	{
		IfExist, C:\Users\%A_UserName%\VIPSystem\settings.ini
		{
			Gui, Font, cGreen bold
			GuiControl, Font, %_ViExistReport%
			GuiControl, Text, %_ViExistReport%, % A_UserName "\VIPSystem(YES)\settings.ini(YES)"
		}
		else
		{
			Gui, Font, cYellow bold
			GuiControl, Font, %_ViExistReport%
			GuiControl, Text, %_ViExistReport%, % A_UserName "\VIPSystem(YES)\settings.ini(NO)"		
		}
	}
	else
	{
		Gui, Font, cRed bold
		GuiControl, Font, %_ViExistReport%
		GuiControl, Text, %_ViExistReport%, % A_UserName "\VIPSystem(NO)\settings.ini(NO)"	
	}	
	Gui, Font, norm
	return
}
OpenContentImport:
{
	IfExist, C:\Users\%A_UserName%\zVIPContent
		IfExist, C:\Users\%A_UserName%\VIPSystem\Content
			FileCopy, C:\Users\%A_UserName%\zVIPContent\*.*, C:\Users\%A_UserName%\VIPSystem\Content
	return
}
UiMove:
{
	PostMessage, 0xA1, 2,,, A 
	return
}
OpenAdmin:
{
	IfExist, C:\Users\%A_UserName%\VIPSystem
	{
		IfWinExist, ahk_exe explorer.exe, C:\Users\%A_UserName%\VIPSystem
		{
			WinActivate, ahk_exe explorer.exe, C:\Users\%A_UserName%\VIPSystem
		}
		else
		{
			Run, explore C:\Users\%A_UserName%\VIPSystem
			WinWait, ahk_exe explorer.exe, C:\Users\%A_UserName%\VIPSystem
		}	
		WinMove,, C:\Users\%A_UserName%\VIPSystem, (A_ScreenWidth - 583), 0, 500, 400
	}	
	else
	{
		IfWinExist, ahk_exe explorer.exe, C:\Users\%A_UserName%
		{
			WinActivate, ahk_exe explorer.exe, C:\Users\%A_UserName%
		}
		else
		{
			Run, explore C:\Users\%A_UserName%\VIPSystem
			WinWait, ahk_exe explorer.exe, C:\Users\%A_UserName%
		}	
		WinMove,, C:\Users\%A_UserName%\VIPSystem, (A_ScreenWidth - 583), 0, 500, 400
	}
	return
}
OpenPF86:
{
	IfWinExist, ahk_exe explorer.exe, C:\Program Files (x86)
	{
		WinActivate, ahk_exe explorer.exe, C:\Program Files (x86)
	}
	else
	{
		Run, explore C:\Program Files (x86)\
		WinWait, ahk_exe explorer.exe, C:\Program Files (x86)
	}	
	WinMove,, Program Files (x86), (A_ScreenWidth - 583), (A_ScreenHeight - 443), 500, 400
	return
}
OpenAppwiz:
{
	IfWinExist, ahk_exe explorer.exe, Programs and Features
	{
		WinActivate, ahk_exe explorer.exe, Programs and Features
	}
	else
	{
		Send {LWin}
		Sleep 50
		Send appwiz.cpl
		Sleep 50
		Send {Control Down}{Shift Down}{Enter}{Shift Up}{Control Up}
		WinWait, ahk_exe explorer.exe, Programs and Features
	}	
	WinMove,ahk_exe explorer.exe, Programs and Features, 330, 20, 550, 750
	; Run %comspec% /c "control appwiz.cpl"
	return
}
OpenNoNotif:
{
	Run, "vss_assets\VSSNoNotif.reg"
	WinWait, ahk_exe regedit.exe, &Yes
	WinActivate, ahk_exe regedit.exe, &Yes
	Sleep 200
	Send, {Enter}
	Sleep 500
	Send, {Enter}
	return
}
OpenFavPins:
{
	Run, "vss_assets\VSSFavPins.vbs",,, pid	
	return
}
OpenPowerPlan:
{
	Gui, +LastFound -AlwaysOnTop
	Run, "vss_assets\VSSPowerScheme.bat",,, pplpid
	WinWait ahk_pid %pplpid%
	WinMove,ahk_pid %pplpid%,, 0, (A_ScreenHeight - 203), 645, 203
	Gui, +LastFound +AlwaysOnTop
	return
}
OpenBCDEdits:
{
	Gui, +LastFound -AlwaysOnTop
	Run, "vss_assets\VSSBCDEdits.bat",,, bcdpid
	WinWait ahk_pid %bcdpid%
	WinMove,,, 0, (A_ScreenHeight - 305), 381, 131
	Gui, +LastFound +AlwaysOnTop
	return
}
OpenInstaller:
{
	Run, "vss_assets\VIPInstaller.exe"
	return
}
OpenVini:
{
	IfWinExist settings.ini - Notepad		
		WinClose settings.ini - Notepad	
	Run Notepad.exe C:\Users\%A_UserName%\VIPSystem\settings.ini
	WinWait, settings.ini - Notepad	
	WinActivate, settings.ini - Notepad
	WinMove, settings.ini - Notepad, , 0, (A_ScreenHeight - 410), 400, 125
	return
}
OpenSystemProp:
{
	Run, sysdm.cpl
	return
}
OpenNetAdapters:
{
	Run, ncpa.cpl
	return
}
OpenDisplay:
{
	Run, desk.cpl
	return
}
OpenServicesOff:
{
	RegWrite, REG_SZ, HKEY_CURRENT_USER, Control Panel\Desktop, MenuShowDelay, 10
	Gui, +LastFound -AlwaysOnTop
	Run, "vss_assets\VSSServices_ALLOFF.bat",,, svcspid
	WinWait ahk_pid %svcspid%
	WinMove,ahk_pid %svcspid%,, 60, (A_ScreenHeight - 603), 445, 503
	Gui, +LastFound +AlwaysOnTop
	return
}
OpenServicesOn:
{
	Gui, +LastFound -AlwaysOnTop
	Run, "vss_assets\VSSServices_ALLON.bat",,, svcspid
	WinWait ahk_pid %svcspid%
	WinMove,ahk_pid %svcspid%,, 60, (A_ScreenHeight - 603), 445, 503
	Gui, +LastFound +AlwaysOnTop
	return
}

LaunchGetStatus:
{
	FileRead, visini, %visiniPath%
	currvLoc := ini_getValue(visini, "Client", "Location")
	currvMach := ini_getValue(visini, "Client", "Machine")
	currvHash := ini_getValue(visini, "Client", "Password")
	for cvidx, cvelm in hashArray
	{
		cvthisval := ini_getValue(bmini, "CommonHashes", cvelm)
		if(currvHash = cvthisval){
				currvPass := cvelm
		}
	}
	if(!currvPass){
				currvPass := "unique/not in quicklist..."
	}	
	currboms := 0
	Loop, Files, C:\Users\All Users\bomgar*, D
	{
		currboms := A_Index
	}	
	GuiControl, Text, %_VIL%, L:
	GuiControl, Text, %_VIM%, M:
	GuiControl, Text, %_VIP%, P:
	GuiControl, Text, %_VIH%, H:
	GuiControl, Text, %_VIB%, #Boms:
	GuiControl, Text, %_VILOC%,  %currvLoc%	
	GuiControl, Text, %_VIMACH%, %currvMach%	
	GuiControl, Text, %_VIHASH%, %currvHash%	
	GuiControl, Text, %_VIPASS%, %currvPass%	
	GuiControl, Text, %_VIBOMN%, %currboms%		
	return
}
CheckSysName:
{
	; Run %comspec% /c "control system"
	IfWinExist System	
		WinClose System
	Send #{Pause}
	WinWait, System	
	WinActivate, System
	WinMove, System, , 25, 25, 750, 310
	Send {Down}{Down}{Down}{Down}
	return
}
GoRadHashes:
{	
	finalHashRad = %A_GuiControl%
	return
}
GoRadMaches:
{
	finalMachRad = %A_GuiControl%
	return
}
GoTimezone:
{
	ChosenTimezone = %A_GuiControl%
	return
}
Open1280:
{
	ChangeResolution( 1280 , 768 )
}
Open1920:
{
	ChangeResolution( 1920 , 1080 )
}
ChangeResolution(sW, sH) {
  VarSetCapacity(dM,156,0)
  NumPut(156,dM,36)
  NumPut(0x5c0000,dM,40)
  NumPut(sW,dM,108)
  NumPut(sH,dM,112)
  DllCall( "ChangeDisplaySettingsA", UInt,&dM, UInt,0 )
	return
}


InitTZ(){
	global
	RegRead, TZKey, HKEY_LOCAL_MACHINE, SYSTEM\CurrentControlSet\Control\TimeZoneInformation, TimeZoneKeyName
	if (TZKey = "Eastern Standard Time"){ 
		ChosenTimezone = East
		GuiControl,, East, 1 
	}
	if (TZKey = "US Eastern Standard Time"){ 
		ChosenTimezone =  Indiana
		GuiControl,, Indiana, 1 
	}
	if (TZKey = "Central Standard Time"){ 
		ChosenTimezone =  Central
		GuiControl,, Central, 1 
	}
	if (TZKey = "Mountain Standard Time"){ 
		ChosenTimezone =  Mountain
		GuiControl,, Mountain, 1 
	}
	if (TZKey = "US Mountain Standard Time"){ 
		ChosenTimezone =  Arizona
		GuiControl,, Arizona, 1 
	}
	if (TZKey = "Pacific Standard Time"){ 
		ChosenTimezone =  Pacific
		GuiControl,, Pacific, 1 
	}
	return
}
LaunchTimezone:
{
	Gui, Submit, NoHide
	if (ChosenTimezone = "East"){ 
		zone = Eastern Standard Time 
	}
	if (ChosenTimezone = "Indiana"){ 
		zone = US Eastern Standard Time 
	}
	if (ChosenTimezone = "Central"){ 
		zone = Central Standard Time 
	}
	if (ChosenTimezone = "Mountain"){ 
		zone = Mountain Standard Time 
	}
	if (ChosenTimezone = "Arizona"){ 
		zone = US Mountain Standard Time 
	}
	if (ChosenTimezone = "Pacific"){ 
		zone = Pacific Standard Time 
	}
	; msgbox,,, % "ChosenTimezone: " ChosenTimezone "`nzone: " zone  ,2	
	commandstring = tzutil /s `"%zone%`"
	Run %comspec% /c %commandstring%
	GuiControl, Text, %_tzreport%, % ">" . zone
	return	
} ;//end//LaunchTimezone

LaunchSysName:
{
	Gui, Submit, NoHide
	namechars := StrLen(NameChoice)
	if(namechars < 16){
		gui,font, c0xc4e5f6 s10 bold, Courier New
		GuiControl, Font, %_NameLen%
		GuiControl, Text, %_NameLen%, %namechars%	
		Gui, Font, cBlack s8 norm, Tahoma

		if(RegExMatch(NameChoice, "^(.*?)(\d\d?)(\w)$", Pa) > 0){
			NewName := Pa1  (Pa2 + 1)  Pa3
		}
		else{
			NewName := NameChoice "2"
		}
		if(NameChoice){
			if (NameChoice != PcName1){	; we didn't accept increment
				if (NameChoice = PcName2){ ; we used previous name
					PcName4 := PcName4 
					PcName3 := PcName3 	
					PcName2 := NameChoice 
					PcName1 := NewName	
				}
				else if (NameChoice = PcName3){
					PcName4 := PcName4 
					PcName3 := PcName1 	
					PcName2 := NameChoice 
					PcName1 := NewName	
				}
				else if (NameChoice = PcName4){
					PcName4 := PcName3 
					PcName3 := PcName1 
					PcName2 := NameChoice 
					PcName1 := NewName	
				}
				else { ;completely unique
					PcName4 := PcName3 
					PcName3 := PcName1 	
					PcName2 := NameChoice 
					PcName1 := NewName	
				}		
			}	
			else { ;we accepted increment
				PcName4 := PcName4 			
				PcName3 := PcName3 	
				PcName2 := NameChoice 
				PcName1 := NewName	
			}
			ini_replaceValue(bmini, "UserVars", "PcName1", PcName1)
			ini_replaceValue(bmini, "UserVars", "PcName2", PcName2)
			ini_replaceValue(bmini, "UserVars", "PcName3", PcName3)
			ini_replaceValue(bmini, "UserVars", "PcName4", PcName4)
			UpdateConfigFile(bminiPath, bmini)

			PcNameString := PcName1 "|" PcName2 "|" PcName3 "|" PcName4
			GuiControl,, NameChoice, |
			GuiControl,, NameChoice, %PcNameString%
			; GuiControl, ChooseString, NameChoice, %PcNameString%
			GuiControl, Choose, NameChoice, 2
			commandstring := "WMIC computersystem where caption='" . A_ComputerName . "' rename '" . PcName2 . "'"
			Run %comspec% /c %commandstring%
			Gui, Font, c0xc4e5f6 s10 bold, Courier New
			GuiControl, Font, %_PcCheck%
			GuiControl, Text, %_PcCheck%,% " """ PcName2 """"
			Gui, Font, cBlack s8 norm, Tahoma
		}
		else {
			font.code
			GuiControl, Font, %_PcCheck%
			GuiControl, Text, %_PcCheck%, no input
		}
	}
	else{
		gui,font, cRed s10 bold, Courier New
		GuiControl, Font, %_NameLen%
		GuiControl, Text, %_NameLen%, %namechars%	
		Gui, Font, cBlack s8 norm, Tahoma
	}
	return
} ;//end//LaunchSysName


LaunchBommaker:
{
	Gui, Submit, NoHide
	if (CityChoice)	{ 
		if (CityChoice != BomCity1)	{ 
			if (CityChoice = BomCity2){ 
				BomCity2 := BomCity1
				BomCity1 := CityChoice
			}
			else if (CityChoice = BomCity3){
				BomCity3 := BomCity1 
				BomCity1 := CityChoice
			}
			else if (CityChoice = BomCity4){
				BomCity4 := BomCity1 
				BomCity1 := CityChoice
			}
			else { ;completely unique BomCity entered, 4th BomCity disappears
				BomCity4 := BomCity3 
				BomCity3 := BomCity2 
				BomCity2 := BomCity1 
				BomCity1 := CityChoice 
			}		
		}	
	}
	ini_replaceValue(bmini, "UserVars", "BomCity1", BomCity1)
	ini_replaceValue(bmini, "UserVars", "BomCity2", BomCity2)
	ini_replaceValue(bmini, "UserVars", "BomCity3", BomCity3)
	ini_replaceValue(bmini, "UserVars", "BomCity4", BomCity4)
	UpdateConfigFile(bminiPath, bmini)
	;; LAUNCH BOMMAKER SCRIPT
	; MSG___(BomLogin BomPassw CityChoice, 3)
	BomDL.init(BomLogin, BomPassw, CityChoice)
	return
} ;//end//LaunchBommaker
CopyMD5:
{
	GuiControlGet, Md5output
	GuiControl, Show, CopyMD5Report
  Clipboard := Md5output
	GuiControl,, CopyMD5Report, % "Copied!"
	Sleep 1000
	GuiControl, Hide, CopyMD5Report
	GuiControl, Hide, Md5output
	return
}

Hashify:
{
	Gui, Submit, NoHide
	GuiControl,, Md5output,   % MD5(Md5input)
	GuiControl, Show, Md5output
	return
}
AskReboot:
{
	Settimer, AskShutdown, OFF
	Settimer, AskReboot, OFF
	ShutdownCode := 6
	GuiControl, Show, %_CONFPOWR%
	GuiControl, , %_CONFPOWR%, % ">Restart"
	ImageButton.Create(_CONFPOWR, EXOpt1, EXOpt2, EXOpt3)
	SetTimer, HideConfirmPower, 2000
	return
}
AskShutdown:
{
	Settimer, AskShutdown, OFF
	Settimer, AskReboot, OFF
	ShutdownCode := 9
	GuiControl, Show, %_CONFPOWR%
	GuiControl, , %_CONFPOWR%, % ">Shutdown"
	ImageButton.Create(_CONFPOWR, EXOpt1, EXOpt2, EXOpt3)
	SetTimer, HideConfirmPower, 2000
	return
}
HideConfirmPower:
{
	Settimer, AskShutdown, OFF
	Settimer, AskReboot, OFF
	GuiControl, Hide, %_CONFPOWR%
	return
}
ConfirmPower:
{
	Shutdown, %ShutdownCode%
	return
}


f9::
GoKiller:
{
	WinGet, id, list,,, Program Manager
	Loop, %id%
	{
		this_id := id%A_Index% 
		WinActivate, ahk_id %this_id%
				WinGetClass, this_class, ahk_id %this_id%
		WinGetTitle, this_title, ahk_id %this_id%
		If(This_class != "Shell_traywnd") && (This_class != "AutoHotkeyGUI") 
		WinClose, ahk_id %this_id%
	}
	return
}

GuiClose: 
GuiEscape:
{
	gui,destroy
	ExitApp
	return
}




;---------------FUNCTIONS-----------------

MakeRadiosFromArray(aArray, aWHX, aY, aInc, aVarPut, aGroupName, aDef = "blank"){	
	local incr
	incr = %aY%
	for idx, elm in aArray 	{    ; MsgBox % "Element number " . A_Index . " is " . Array[A_Index]
	Gui, Add, Radio, % ( aDef = elm ? "Checked" : "" ) " " aWHX " y"incr " " aVarPut idx " " aGroupName " 0x1000",	%elm%
	incr := incr + aInc
	}
	incr = 0
	return
}
UpdateConfigFile(Path, ByRef Content)
{
	FileDelete, %Path%
	FileAppend, %Content%, %Path%
	Return
}
; TestUniqueDrive(){
		; if(LastDriveSerial = DriveSerial){
		; return false
		; }
		; else{
		; return true
		; }
; }

HideFocusBorder(wParam, lParam := "", uMsg := "", hWnd := "") {
   ; WM_UPDATEUISTATE = 0x0128
	Static Affected := [] ; affected controls / GUIs
        , HideFocus := 0x00010001 ; UIS_SET << 16 | UISF_HIDEFOCUS
	     , OnMsg := OnMessage(0x0128, Func("HideFocusBorder"))
	If (uMsg = 0x0128) { ; called by OnMessage()
      If (wParam = HideFocus)
         Affected[hWnd] := True
      Else If Affected[hWnd]
         PostMessage, 0x0128, %HideFocus%, 0, , ahk_id %hWnd%
  }
  Else If DllCall("IsWindow", "Ptr", wParam, "UInt")
	  PostMessage, 0x0128, %HideFocus%, 0, , ahk_id %wParam%
}

f11::listvars
f8::Pause
f3::Reload

#Include vss_includes/incl_CreateConfigFile.ahk
#Include vss_includes/incl_LaunchSettings.ahk
#Include vss_includes/incl_BomDL.ahk
#Include vss_includes/incl_md5.ahk
